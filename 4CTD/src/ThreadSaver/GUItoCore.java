package ThreadSaver;

import java.util.ArrayList;

public interface GUItoCore {
	ArrayList<String> getThreadKeys();
	
	ArrayList<String> getImageKeys(String threadkey);
	
	int getPercentDone(String imagekey, String threadkey);
	
	String getStatus(String imagekey, String threadkey);
	
	String save(String urli);
}
