package ThreadSaver;

public class Site {
	public static final int 
		SITE_IS_4CHAN = 0,
		SITE_IS_4CHANARCHIVE= 1;
	
	public static int determineSite(String url) {
		if(url.contains("4chanarchive.org")) {
			return SITE_IS_4CHANARCHIVE;
		} else {
			return SITE_IS_4CHAN;
		}
	}
	
	public static String getImageParseString(int SITE_IS) {
		String compare;
		switch (SITE_IS) {
		case SITE_IS_4CHANARCHIVE:
			compare = "<a href=\"http://4chanarchive.org/images/";
			break;
		case SITE_IS_4CHAN:
		default:
			compare = "<a href=\"http://images.4chan.org/";
			break;
		}
		return compare;
	}
	
	public static int getImageParseOffset(int SITE_IS) {
		int offset;
		switch (SITE_IS) {
		case SITE_IS_4CHANARCHIVE:
			offset = 9;
			break;
		case SITE_IS_4CHAN:
		default:
			offset = 9;
			break;
		}
		return offset;
	}
	
}
