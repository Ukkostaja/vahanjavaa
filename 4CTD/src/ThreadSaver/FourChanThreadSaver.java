package ThreadSaver;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.net.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Timer;
import java.util.Map.Entry;
import java.util.logging.Level;

import javax.swing.UIManager;

public class FourChanThreadSaver extends Thread implements GUItoCore{
	NetworkClient clientNetwork;
	NetworkServer serverNetwork;
	ArrayList<Timer> aikalista;
	Settings settings;
	HashMap<String, ThreadWorker> threads;
	GUI gui;
	/**
	 * @param args
	 */
	
	public FourChanThreadSaver() {
		super();
		this.settings = new Settings();
		this.threads = new HashMap<String, ThreadWorker>();
		this.loadThreadInfo();
		switch (settings.mode) {
		case 0:
			this.serverNetwork = new NetworkServer(this);
			break;
		case 2:
			this.clientNetwork = new NetworkClient(settings);
			this.gui = new GUI(settings, clientNetwork);
			break;
		case 1:
		default:
			this.gui = new GUI(settings,this);
			break;
		}


		
		
		this.aikalista = new ArrayList<Timer>();
		
		//this.startSaved();
	}
	
	
	public void startSaved() {
		ThreadWorker th;
		Iterator<Entry<String,ThreadWorker>> it = threads.entrySet().iterator();
		while(it.hasNext()) {
			th = it.next().getValue();
			if (th != null) {
				//th.start();
			}
		}
		
	}


	private void saveThreadInfo() {
		FileWriter fw;
		Iterator<Entry<String, ThreadWorker>> it = threads.entrySet().iterator();
		URL url;
		try {
			fw = new FileWriter("thread.list");//settings.ThreadList);
			while(it.hasNext()) {
				url = it.next().getValue().url;
				if (url != null) {
					fw.write(url.toString() + "\n");
				}
				
			}
			fw.close();
		} catch (Exception e) {
			settings.logger.throwing("FourChanThreadSaver", "saveThreadInfo", e);
			//e.printStackTrace();
		}
		
	}
	
	private void loadThreadInfo() {
		BufferedReader br;
		String str;
		try {
			br = new BufferedReader(new FileReader(settings.ThreadList));
			while( (str = br.readLine()) != null){
				startWorker(str);
				
			}
		} catch (FileNotFoundException fnf) {
			settings.logger.logp(Level.FINER, "FourChanThreadSaver", "loadThreadInfo", "", fnf);
		} catch (NullPointerException nulpo) {
			settings.logger.logp(Level.FINER, "FourChanThreadSaver" ,"loadThreadInfo","",nulpo);
		} catch (Exception e) {
			settings.logger.throwing("FourChanThreadSaver", "loadThreadInfo", e);
			//e.printStackTrace();
		}
	}
	
	public String startWorker(String urli){
		try {
			return startWorker(urli, settings);
		} catch (Exception e) {
			settings.logger.throwing("FourChanThreadSaver", "startWorker", e);
			e.printStackTrace();
		}
		settings.logger.warning("startWorker did not return normally or throw an exception.");
		return "1";
	}
	
	public String startWorker(String urli, Settings settings) throws Exception{
		    // Create a URL for the desired page
		    URL url = new URL(urli);
		    
		    ThreadWorker worker = new ThreadWorker(url,settings);
		    String name = worker.getThreadNro().getName();
		    if(threads.containsKey(name)) return null;
		    threads.put(name, worker);
		    this.saveThreadInfo();
		    worker.start();
		    return name;
		    
	}

	@Override
	public ArrayList<String> getThreadKeys() {
		ArrayList<String> keys = new ArrayList<String>(threads.size());
		Iterator<Entry<String,ThreadWorker>> it = threads.entrySet().iterator();
		while(it.hasNext()) {
			keys.add(it.next().getKey());
		}
		return keys;
	}


	@Override
	public ArrayList<String> getImageKeys(String threadkey) {
		ThreadWorker chosen = threads.get(threadkey);
		
			return chosen.getImageKeys();
		
		//return null;
	}


	@Override
	public int getPercentDone(String imagekey, String threadkey) {
		int arvo;
		try {
			arvo = this.threads.get(threadkey).percentDone(imagekey);
			settings.logger.finest(threadkey+ "/"+imagekey +" = " +arvo);
		} catch (NullPointerException nulpo) {
			arvo = -1;
			settings.logger.throwing("FourChanThreadSaver", "getPercentDone", nulpo);
		}
		return arvo;
	}
	
		@Override
	public String save(String urli) {
			return startWorker(urli);
		// TODO Auto-generated method stub
		
	}
		
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		 try {
			    // Set cross-platform Java L&F (also called "Metal")
		        UIManager.setLookAndFeel(
		        		UIManager.getSystemLookAndFeelClassName()
		            //UIManager.getCrossPlatformLookAndFeelClassName()
		            );
		    } catch (Exception e) {
		    	System.out.println("Look and feel failed. Unable to start.");
		    	System.exit(1);
		    }
		FourChanThreadSaver cts = new FourChanThreadSaver();
	}


	@Override
	public String getStatus(String imagekey, String threadkey) {
		String arvo;
		try {
			arvo = this.threads.get(threadkey).status(imagekey);
			settings.logger.finest(threadkey+ "/"+imagekey +" = " +arvo);
		} catch (NullPointerException nulpo) {
			arvo = "Null Pointer";
			settings.logger.throwing("FourChanThreadSaver", "getPercentDone", nulpo);
		}
		return arvo;

	}



}
