package ThreadSaver;
//koe2
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import javax.swing.*;

public class GUI extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Settings settings;
	GUItoCore father;
	Container pane;
	ActionListener Do;
	JLabel status;
	HashMap<String,Line> lines;
	Timer mastertimer;
	Timer progresstimer;
	JPanel result;
	HashMap<String, ArrayList<String>> keys;
	
	public GUI(Settings settings, GUItoCore father) {
		super("4Chan Thread Saver");
		this.settings = settings;
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		this.setMinimumSize(new Dimension(640, 480));
		this.father = father;
		this.lines = new HashMap<String, Line>();
		this.keys = new HashMap<String, ArrayList<String>>(20);
		
		JTextField parseThis = new JTextField("http://boards.4chan.org/hr/res/1280139");
		//JTextArea result = new JTextArea("lololol");
		
		
		GridBagLayout muut = new GridBagLayout();
	
		GridBagConstraints mg = new GridBagConstraints();

		result = new JPanel();
		BoxLayout bl = new BoxLayout(result, BoxLayout.PAGE_AXIS);
		
		result.setLayout(bl);
		
		JScrollPane scrollableResult = new JScrollPane(result);

		
		
		this.status = new JLabel("Min� olen status.");
		Line line = new Line("a", "b");
		line.setVisible(true);
		//scrollableResult.add(line);
		/*
		result.setPreferredSize(new Dimension(200,100));
		result.setVisible(true);*/
		this.Do = new Actioner(this,parseThis, status);
		/*
		parseThis.setMinimumSize(new Dimension(200,parseThis.getSize().height ));
		parseThis.setPreferredSize(parseThis.getMinimumSize());
		//parseThis.addActionListener(Do);
		parseThis.setVisible(true);
		*/
		JButton button = new JButton("Hae");
		button.addActionListener(Do);
		
		GridBagLayout flo = new GridBagLayout();
		GridBagConstraints gBC = new GridBagConstraints();
		gBC.fill = GridBagConstraints.HORIZONTAL;
		this.pane = getContentPane();
		this.pane.setLayout(flo);
        
		gBC.weightx = 0.5;
        gBC.gridx = 0;
        gBC.gridy = 0;
        pane.add(parseThis, gBC);
        
        
        gBC.weightx = 0;
        gBC.gridx = 1;
        gBC.gridy = 0;
        pane.add(button, gBC);

        gBC.fill = GridBagConstraints.BOTH;
		
        gBC.anchor = GridBagConstraints.CENTER;
        gBC.weightx = 1;
        gBC.weighty= 1;
        gBC.gridwidth = 2;
        gBC.gridx = 0;
        gBC.gridy = 1;
        pane.add(scrollableResult, gBC);
        
        gBC.fill = GridBagConstraints.VERTICAL;
        gBC.anchor = GridBagConstraints.SOUTH;
        gBC.weighty = 0.0;
        gBC.gridx = 0;
        gBC.gridy = 2;
        pane.add(status, gBC);
        /*
        JPanel panel = new JPanel();

		JPanel top = new JPanel(flo);
		JPanel center = new JPanel();
		JPanel bottom = new JPanel(flo);
		top.add(parseThis);
		top.add(button);
		center.add(result);
		bottom.add(status);
		
		panel.setLayout(flo);
		panel.add(top);
		panel.add(center );
		panel.add(bottom);
		*/

		//this.repaint();
		
		this.mastertimer = new Timer(4000, new Updater(this,false));
		this.mastertimer.start();
		this.progresstimer = new Timer(1000, new Updater(this, true));
		this.updateAll();
		
		this.pack();
		this.setVisible(true);
		
	}

	public void updateProgress() {
		Entry<String,ArrayList<String>> entry;
		Iterator<Entry<String,ArrayList<String>>> it = keys.entrySet().iterator();
		while(it.hasNext()){
			entry = it.next();
			if(entry != null) {
				String threadkey = entry.getKey();
				ArrayList<String> imagekeys = entry.getValue();
				for(int i = 0; i< imagekeys.size(); i++) {
					String imagekey = imagekeys.get(i);
					Line line = lines.get(threadkey+"/"+imagekey);
					if(line != null) {
						settings.logger.info(line.toString());
						line.setProgress(father.getPercentDone(imagekey, threadkey));
						line.setStatus(father.getStatus(imagekey, threadkey));
					}
				}
			}
		}
	}
	
	public void updateAll(){
		GridBagConstraints mg = new GridBagConstraints();
		mg.fill = GridBagConstraints.HORIZONTAL;
		mg.weighty = 0.1;
		mg.weightx = 1.0;
		mg.anchor = mg.LINE_START;
		BorderLayout bl = new BorderLayout();
		settings.logger.entering("GUI", "updateAll");
		ArrayList<String> threadkeys;
		threadkeys = this.father.getThreadKeys();
		for(int i = 0; i< threadkeys.size();i++) {
			String threadkey = threadkeys.get(i);
			ArrayList<String> imagekeys= this.father.getImageKeys(threadkey);
			for(int j = 0; j < imagekeys.size(); j++) {
				String imagekey = imagekeys.get(j);
				String key = threadkey+"/"+imagekey;
				Line line = new Line(threadkey,imagekey);
				if (!lines.containsKey(key)) {
					int progress= this.father.getPercentDone(imagekey, threadkey);
					if (progress != -1){
						settings.logger.finest(threadkey+ "/" +imagekey + " added to GUI");
						line.setProgress(progress);
						line.setStatus(father.getStatus(imagekey, threadkey));
						line.setVisible(true);
						//JPanel pane = new JPanel(bl);
						//pane.add(line);
						this.lines.put(key,line);
						result.add(line);
					}
				}
			}
			this.keys.put(threadkey, this.father.getImageKeys(threadkey));
		}
		result.revalidate();
		settings.logger.exiting("GUI", "updateAll");
		
		
	}
	
	public String getKey(String threadkey, String imagekey){
		return threadkey+"/"+imagekey;
	}
	
	public void addThread(String urli) {
		
		String threadkey = father.save(urli);
		if (threadkey == null) return;
		System.out.println("keyis" +threadkey+  " right");
		ArrayList<String> imagekeys = father.getImageKeys(threadkey);
		System.out.println(imagekeys.toString());
		keys.put(threadkey, imagekeys);
		Line rivi;
		for (int i = 0; i < imagekeys.size(); i++) {
			String imagekey = imagekeys.get(i);
			if(imagekey != null) {
				String key =getKey(threadkey,imagekey);
				rivi = new Line(key);
				lines.put(key,rivi);
				result.add(rivi);
			}
		}
	}
	
	public void errorReport(Exception e) {
		status.setText(e.toString());
	}
}

class MyMouseListener extends MouseAdapter { 

	public MyMouseListener() {} 

	public void mouseClicked(MouseEvent e) { 

		if (e.getButton() == MouseEvent.BUTTON3) { 
			
		} 
	} 

}

class Updater implements ActionListener{
	GUI tama;
	boolean progressOnly;
	
	public Updater(GUI tama, boolean progressOnly) {
		this.tama = tama;
		this.progressOnly = progressOnly;
		// TODO Auto-generated constructor stub
	}
	
	public void actionPerformed(ActionEvent e) {
		if (this.progressOnly) {
			tama.updateProgress();
		} else {
			tama.updateAll();
		}
	}
}

class Actioner implements ActionListener{
	final GUI gui;
	JTextField parseThis;
	JTextArea result;
	JLabel status;
	
	Actioner(GUI gui, JTextField parseThis, JLabel status){
		this.gui = gui;
		this.parseThis = parseThis;
		this.status = status;
	}
	
	public void actionPerformed(ActionEvent e){
		try {
			status.setText("Started get.");
			gui.addThread(parseThis.getText());
			//father.startWorker(parseThis.getText(),settings);
			//result.setText(tulos);
			status.setText("Get successful.");
		} catch (Exception exc) {
			exc.printStackTrace();
			status.setText(exc.toString());
			System.out.print(exc);
		}
	}
}