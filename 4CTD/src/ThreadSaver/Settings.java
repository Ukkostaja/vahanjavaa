package ThreadSaver;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.InetAddress;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.logging.*;

public class Settings {
	final static String filename = "config";
	
	Logger logger;
	int mode; // 0 = daemon, 1 = normal, 2 = head
	InetAddress ip;
	int port;
	int NroImageThreads;
	int NroThreadThreads;
	String SaveFolder;
	String ThreadList;
	int floodtimeout; // addtorest. used by sleep
	
	HashMap<String, String> map;
	boolean success;
	
	public Settings() {
		map = new HashMap<String, String>(2);
		map.put("mode", null);
		map.put("ip", null);
		map.put("port", null);
		map.put("NroImageThreads", null);
		map.put("NroThreadThreads", null);
		map.put("SaveFolder", null);
		map.put("ThreadList", null);
		map.put("floodtimeout", null);
		
		this.logger = Logger.getLogger("ThreadSaver");
		
		success = true;
		this.configLogger();
		this.readFromFile();
		
	}
	
	private void configLogger() {
		  try {

		      // This block configure the logger with handler and formatter
			  FileHandler fh = new FileHandler("4CTD.log", false);
		      logger.addHandler(fh);
		      logger.setLevel(Level.ALL);
		      SimpleFormatter formatter = new SimpleFormatter();
		      fh.setFormatter(formatter);

		      // the following statement is used to log any messages   
		      //logger.log(Level.WARNING,"It Works");

		    } catch (SecurityException e) {
		      e.printStackTrace();
		    } catch (IOException e) {
		      e.printStackTrace();
		    }
	}
	
	private void restoreDefaults() {
		mode = 1;
		floodtimeout = 10000;
		NroImageThreads = 1;
		NroThreadThreads = 1;
		SaveFolder = "images/";
		ThreadList = "thread.list";
		try {
			ip = InetAddress.getByName("127.0.0.1");
			port = 44444;
		} catch (Exception e) {
			e.printStackTrace();
			// TODO: handle exception
		}
	}
	
	private HashMap<String, String> convertToHashMap() {
		map.put("mode", Integer.toString(mode));
		map.put("ip", ip.getCanonicalHostName());
		map.put("port", Integer.toString(port));
		map.put("NroImageThreads", Integer.toString(NroImageThreads ));
		map.put("NroThreadThreads", Integer.toString(NroThreadThreads ));
		map.put("SaveFolder", SaveFolder);
		map.put("ThreadList", ThreadList);
		map.put("floodtimeout", Integer.toString(floodtimeout));
		
		
		return map;
	}
	
	private void openHashMap() {
		try {
			this.mode = Integer.parseInt(map.get("mode"));
			this.ip = InetAddress.getByName(map.get("ip"));
			this.port = Integer.parseInt(map.get("port"));
			this.NroImageThreads = Integer.parseInt(map.get("NroImageThreads"));
			this.NroThreadThreads = Integer.parseInt(map.get("NroThreadThreads"));
			this.SaveFolder = map.get("SaveFolder");
			this.ThreadList = map.get("ThreadList");
			this.floodtimeout = Integer.parseInt(map.get("floodtimeout"));
			
			
		} catch (Exception e) {
			this.restoreDefaults();
			success = false;
		}
	}
	
	private boolean validate() {
		Entry<String, String> entry;
		Iterator<Entry<String, String>> it = map.entrySet().iterator();
		while (it.hasNext()) {
			entry = it.next();
			if (entry.getValue() == null) return false;
		}
		return true;
	}
	

	
	public boolean readFromFile(){
		this.logger.entering("Settings", "readFromFile");
		int separator;
		String line,linekey,linevalue;
		try{
			BufferedReader br = new BufferedReader(new FileReader(filename));
			while( ((line =br.readLine()) != null) && ((separator = line.indexOf("=")) != -1)){
				this.logger.config(line);
				linekey = line.substring(0, separator);
				linevalue = line.substring(separator+1);
				if(map.containsKey(linekey)) {
					map.put(linekey, linevalue);
				}
			}

			this.openHashMap();

		} catch (FileNotFoundException fnfe) {
			this.logger.logp(Level.FINER, "Settings", "readFromFile", "Config file not found.");
			this.restoreDefaults();
			this.saveToFile();
			this.logger.exiting("Settings", "readFromFile");
			return true;
		} catch (Exception e) {
			this.logger.throwing("Settings", "readFromFile",e);
			//e.printStackTrace();
			this.logger.exiting("Settings", "readFromFile");
			return false;
		}
		this.logger.exiting("Settings", "readFromFile");
		return true;
	}
	
	public void saveToFile() {
		FileWriter fw;
		HashMap<String, String> map = convertToHashMap();
		Entry<String, String> entry;
		Iterator<Entry<String, String>> it = map.entrySet().iterator();
		try {
			fw = new FileWriter(filename);
			while (it.hasNext()) {
				entry = it.next();
				fw.write(entry.getKey() + "=" + entry.getValue() + "\n");
			}
			fw.close();
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
}
