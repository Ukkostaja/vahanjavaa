package ThreadSaver;

//import java.awt.image.BufferedImage;
import java.io.*;
import java.net.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;

//import javax.imageio.ImageIO;

public class ThreadWorker extends Thread {
	Settings settings;
	
	URL url;
	HashMap<String, ImageWorker> kuvat;
	String result;
	Exception error;
	boolean isError;
	int status;
	File threadnro;
	int SITE_IS;
	boolean daemon;
	Queue<ImageWorker> jono;
	ArrayList<ImageWorker> active;
	int nroDone;

	
	
	
	//String result;
	
	public ThreadWorker(URL url, Settings settings){
		super();
		this.settings = settings;
		
		this.url = url;
		System.out.println(url.getHost());
		this.SITE_IS = Site.determineSite(url.getHost()); 
		
		this.threadnro = parseThreadNro(url);
		kuvat = new HashMap<String, ImageWorker>(50);
		this.result = new String("");
		this.isError = false;
		this.status = 0;

		this.active = new ArrayList<ImageWorker>(settings.NroImageThreads);
		this.jono = new ConcurrentLinkedQueue<ImageWorker>();
		this.nroDone =0;
	}
	
	public void updateSettings(Settings settings) {
		this.settings = settings;
		Iterator<Entry<String, ImageWorker>> it = kuvat.entrySet().iterator();
		try {
			while(it.hasNext()) {
				it.next().getValue().updateSettings(settings);
			}
		} catch (NullPointerException e) {
			settings.logger.throwing("ThreadWorker", "updateSettings", e);
			//e.printStackTrace();
		}
	}
	
	public File getThreadNro(){
		return this.threadnro;
	}
	
	private File parseThreadNro(URL url) {
		String kansio = parseThreadNroFromSite(url);
		File tamakansio = new File(settings.SaveFolder + kansio);
		settings.logger.info("Did make folder: "+ tamakansio.getAbsolutePath() + "? " + tamakansio.mkdirs()); //mkdirs command must be run in this method.
		//System.out.println(tamakansio.mkdirs());
		return tamakansio;
	}
	
	public void run() {
		settings.logger.entering("ThreadWorker", "run", this.getThreadNro().getName());
		readURL();
		saveImages();
		processQue();
		settings.logger.exiting("ThreadWorker", "run", this.getThreadNro().getName());
	}
	
	public void run_2() {
		switch (this.status) {
		case 0:
			System.out.println("0");
			readURL();
			break;
		case 1:
			System.out.println("1");
			saveImages();
			break;
		case 2:
			System.out.println("2");
			processQue();
			break;
		case 3:
			System.out.println("3");
			System.out.println("Ei kuitenkaan onnistu");
		default:
			break;
		}
	}
	
	public void processQue() {
		ImageWorker tama;
		settings.logger.entering("ThreadWorker", "processQue");
		while(!jono.isEmpty()) {
			if (active.size() <= settings.NroImageThreads) {
				tama = jono.poll();
				if (tama != null) {
					active.add(tama);
					tama.start();
				}
			}
			settings.logger.info("Thread "+ this.threadnro.getName() + " has " + jono.size()+" in que.");
			try {
			Thread.sleep(500);
			} catch (Exception e) {
				settings.logger.throwing("ThreadWorker", "processQue", e);
				//e.printStackTrace();
			}
			for(int i = 0; i<active.size();i++) {
				tama = active.get(i);
				//System.out.println("active size " + active.size());
				if (tama != null) {
					if (tama.isDone()) {
						//System.out.println(tama.filename + " done");
						this.nroDone++;
						settings.logger.info(this.nroDone + " / "+ this.kuvat.size() + " done");
						active.remove(i);
					}
					if (tama.SEerror) {
						try {
							tama.nuku();
						} catch (Exception e) {
							//System.out.println("image lost " +tama.filename);
							settings.logger.throwing("ThreadWorker", "processQue", e);
							//e.printStackTrace();
						}
					}
						
					
					
				}
			}
			
		} settings.logger.exiting("ThreadWorker", "processQue");
		
	}
	
	public void reset() {
		if (this.daemon) {
			this.status = 0;
		}
	}
	
	public void readURL(){
		settings.logger.entering("ThreadWorker", "readURL", this.threadnro.getName());	
		    // Read all the text returned by the server
		try {
		    BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
		    String str;
		    long counter = 0;
		    while ((str = in.readLine()) != null) {
		        parseURL(str);
		        counter++;
		        if (counter % 50 == 0) { 
		        	//System.out.println(counter);
		        	//Thread.sleep(5);
		        }
		        	// str is one line of text; readLine() strips the newline character(s)
		    }
		    
		    in.close();
		    this.status = 1;
		} catch (FileNotFoundException fnfe) { //if the thread 404's
			//System.out.println(this.threadnro.getName() + " Thread 404");
			settings.logger.info(this.threadnro.getName() + " Thread 404");
			this.status = -1;
		} catch (Exception e) {
			settings.logger.throwing("ThreadWorker", "readURL", e);
			//e.printStackTrace();
		}
		settings.logger.exiting("ThreadWorker", "readURL", this.threadnro.getName());
	}
	
	public String parseThreadNroFromSite(URL url) {
		if (this.SITE_IS == Site.SITE_IS_4CHANARCHIVE) {
			String thread_id= "thread_id=";
			String urli = url.getQuery();
			int headerindex = urli.indexOf(thread_id);
			int kohta = headerindex + thread_id.length();
			int loppukohta = urli.indexOf("&", kohta) -1;
			return urli.substring(kohta, loppukohta);
		} else {
			return parseFileName(url);
		}
	}
	
	public void parseURL(String str) {
		int cursor = 0;
		int kohta = 0;
		int tokakohta;
		String dublicate = "";
		int offset = Site.getImageParseOffset(this.SITE_IS);
		String compare = Site.getImageParseString(this.SITE_IS);
		//System.out.println(compare);
		try {
			while((kohta =str.indexOf(compare, cursor)) != -1){
				kohta+= offset;
				tokakohta = str.indexOf("\"", kohta);
				cursor = tokakohta;
				String subString = str.substring(kohta, tokakohta);
				//System.out.println(subString);
				if ((subString.compareTo(dublicate)) != 0 ) {
					dublicate = subString;
					URL url = new URL(subString);
					ImageWorker imagesaver = new ImageWorker(url, this, settings);
					kuvat.put(ImageWorker.parseFileName(url),imagesaver);
					jono.offer(imagesaver);
				}
				//Thread.sleep(5);
			
			}
		} catch (MalformedURLException e) {
			settings.logger.throwing("ThreadWorker", "parseURL", e);
			//e.printStackTrace();
		} catch (Exception e) {
			settings.logger.throwing("ThreadWorker", "parseURL", e);
			//e.printStackTrace();
		}
		//this.status = 2;
		//System.out.println("olen t��ll�!");
	}
	
	public int percentDone(String imagekey) throws NullPointerException{
		ImageWorker worker = kuvat.get(imagekey);
		if (worker.isAlive()){
			return worker.percentDone();
		} else {
			return 100;
		}
	}
	
	public String status(String imagekey) throws NullPointerException{
		ImageWorker worker = kuvat.get(imagekey);
		if (worker.isAlive()){
			return worker.getStatus();
		} else {
			return "Dead";
		}
	}
	
	/**
	 * 
	 * @return
	 */
	public ArrayList<String> getImageKeys(){
		String key;
		ArrayList<String> keys = new ArrayList<String>(kuvat.size());
		//settings.logger.info("kuvat.size = " + kuvat.size());
		Iterator<Entry<String,ImageWorker>> it = kuvat.entrySet().iterator();
		while(it.hasNext()) {
			key = it.next().getKey();
			keys.add(key);
			//System.out.println(it.next().getKey());
		}
		return keys;
	}
	
	private static String parseFileName(URL url) {
		String filepath = url.getFile();
		int index = filepath.lastIndexOf("/");
		String filename;
		if (index != -1) {
			index++;
			filename = filepath.substring(index);
		} else {
			filename = null;
		}
		//System.out.println(filename);
		return filename;
	}
	
	public void saveImages() {
		settings.logger.entering("ThreadWorker", "saveImages");
		ImageWorker url;

		
		String filename = "";
		for(int i=0; i < kuvat.size(); i++) {
			if ((url = kuvat.get(i)) != null) {
				//active.add(kuvat.get(i));
			}
		}
		this.status = 2;
		settings.logger.exiting("ThreadWorker", "saveImages");
		
	}
	
}
