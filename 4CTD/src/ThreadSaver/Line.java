package ThreadSaver;

import java.awt.Color;

import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.JSplitPane;

public class Line extends JSplitPane{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	JProgressBar progress;
	String thread,image;
	boolean isThread;
	
	
	
	public Line(String thread, String image, JProgressBar bar) {
		super(JSplitPane.HORIZONTAL_SPLIT, new JLabel(thread + "//" + image), bar);
		this.progress = bar;
		this.thread = thread; this.image = image;
		this.progress.setString("0");
		this.progress.setStringPainted(true);

		//bar.setIndeterminate(true);
	}
	
	public long getThread() {
		return Long.getLong(thread);
	}
	
	public long getImage() {
		return Long.getLong(image);
	}
	
	public void setStatus(String status) {
		this.progress.setString(status + " " + Integer.toString(this.progress.getValue()));
	}
	
	public void setError(){
		this.progress.setString("ERROR");
		this.progress.setForeground(Color.RED);
	}
	
	public void setProgress(int v) {
		this.progress.setValue(v);
		//this.progress.setString(v + " %");
	}
	
	public Line(String thread, String image) {
		this(thread, image, new JProgressBar());
	}
	
	public Line(String key) {
		this(key.substring(0,key.lastIndexOf("/")),key.substring(key.lastIndexOf("/")+1));
		
		
	}


	
}
