package ThreadSaver;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.net.SocketException;
import java.net.URL;
import java.net.URLConnection;

public class ImageWorker extends Thread {
	Settings settings;
	URL url;
	URLConnection conn;
	int size;
	int progress;
	ThreadWorker thread;
	String filename;
	boolean SEerror;
	String status;

	
	ImageWorker(URL url, ThreadWorker thread, Settings settings) {
		this.status = "Born";
		this.settings = settings;
		this.url = url;
		this.thread = thread;
		this.size = 0;
		progress = 0;
		this.SEerror = true;
		this.status = "Initializing";

	}
	
	public void updateSettings(Settings settings) {
		this.settings = settings;
	}
	
	public boolean isDone() {
		if (progress == size) {
			settings.logger.finer(this.filename + " is done.");
			return true;
		} else {
			settings.logger.finest(this.filename + " is NOT done.");
			return false;
		}
	}
	
	public int percentDone() {
		if (size == 0) {
			return -1;
		} else {
			return ((progress *100 )/ size);
		}
	}
	
	public String getStatus(){
		return this.status;
	}
	
	private String getSaveFolder() {
		return thread.getThreadNro().getPath();
	}
	
	private void connectAndDetermineSize() throws Exception{
		conn= url.openConnection();
		this.size = conn.getContentLength();
	}
	
	public void nuku() {
		try {
			Thread.sleep(settings.floodtimeout);
		}catch (Exception e) {
			settings.logger.throwing("ImageWorker", "nuku", e);
			// TODO: handle exception
		}
		
	}
	
	public void work() {
		try {
			this.connectAndDetermineSize();
			this.privateParseFileName(url);
			if(this.filename == null) {
				progress = -1;
				return;
			} else {
				progress = 0;
			}
			this.status = "Downloading";
			File tiedosto = new File( this.getSaveFolder()+"/"+ filename);
			//System.out.println(tiedosto.getAbsolutePath());
			if (tiedosto.length() != (long)size) {
				
				FileOutputStream bos = new FileOutputStream(tiedosto);
				BufferedInputStream bis = new BufferedInputStream(conn.getInputStream());
				byte[] b = new byte[size];
				while (true) {
			         synchronized (b) {
			            int amountRead = bis.read(b);
			            if (amountRead == -1) {
			               break;
			            }
			            progress+= amountRead;
			            bos.write(b, 0, amountRead); 
			         }
					//System.out.println(filename +":"+progress +" / " + size + " is " + (progress*100) / (size*100));
					//Thread.sleep(5);
					//this.wait(1);
				}
				
				bos.flush(); 
				bis.close();
				bos.close();
				settings.logger.info("Saved image:" +filename);
				this.status = "Saved to Disk";
				this.SEerror = false;
			} else {
				progress = size;
				settings.logger.info("Skipped file:" + filename + " Already complete!");
				this.status = "Skipped";
				this.SEerror = false;
			}
			
		} catch (SocketException se) {
			settings.logger.throwing("ImageWorker", "work", se);
			this.status = "SE Exception";
			//System.out.println("SE in "+ filename);
			this.SEerror = true;
		}
		catch (Exception e) {
			settings.logger.throwing("ImageWorker", "work", e);
			e.printStackTrace();
		} 
	}
	public void run(){
		settings.logger.entering("ImageWorker", "run");
		int seCounter= 0;
		this.status ="Running";
		while(this.SEerror) {
			this.work();
			settings.logger.finest("ImageWorker " + this.filename +" bouncenro: "+ seCounter++);
		}
		this.status = "Run Complete";
		settings.logger.exiting("ImageWorker " +this.filename, "run");
	}
	
	public static String parseFileName(URL url) {
		
		String tempfilename;
		String filepath = url.getFile();
		int index = filepath.lastIndexOf("/");

		if (index != -1) {
			index++;
			tempfilename = filepath.substring(index);
		} else {
			System.out.println("URL FILEPATH IS "+ filepath);
			tempfilename = null;
		}
		return tempfilename;
	}
	
	private void privateParseFileName(URL url) {
			this.status = "Parsing URL";
			this.filename = parseFileName(url);

	}

}
