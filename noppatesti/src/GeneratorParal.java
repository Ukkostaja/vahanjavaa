import java.util.Random;


public class GeneratorParal extends Thread {
	Random rand;
	Settings set;
	Tulos res;
	long t;
	long startTime;
	int index;
	long cdeep;
	
	
	public GeneratorParal(int index, Settings set,Tulos result){
		this.index = index;
		this.rand = new Random();
		this.set = set;
		this.res = result;
	}

	public void report() {
		System.out.println("Toisto " + t );
	}
	
	public int rollOne() {
		return this.rand.nextInt(set.die) + 1;
	}
	
	public void rollDetermination(int a){
		if(a >= set.target) res.success +=1;
		if(a >= set.dtarget) {
			if(set.explodingTens == true) {
				if(cdeep < set.deep){
					cdeep++;
					this.rollDetermination(this.rollOne());
				}
			} else {
				res.success +=1;
			}
		}
	}
	
	public void roll(){
		int a = 0;
		for(int i = 0; i < set.roll ; i++ ){
			cdeep = 0;
			this.rollDetermination(this.rollOne());
			
		}
	}
	
}
