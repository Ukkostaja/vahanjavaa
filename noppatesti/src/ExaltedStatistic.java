import java.util.ArrayList;
import java.util.Random;
import java.util.List;

public class ExaltedStatistic {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int toisto = 10000000;
		long success = 0;
		int die = 10;
		int roll = 10;
		int target = 7;
		int dtarget= 10;
		long reportIntervalNano = 3000000000L; // on 3 s.
		//List<Integer> tupla = tuplaSuccess(dtarget, die);
		final long startTime = System.nanoTime();
		long tempTime = startTime;
		for(int t = 0;t<toisto;t++){
			if (System.nanoTime() - tempTime >= reportIntervalNano){
					System.out.println("Toisto numero  "+ t);
					System.out.println("/             "+ toisto);
					tempTime = System.nanoTime();
			}
			//long count = 0;
			Random rand = new Random();
			int a=0;
			for(int i = 0; i < roll ; i++ ){
				a = rand.nextInt(die) + 1;
				//System.out.print(a + " ");
				if(a >= target) success +=1;
				for(int j = dtarget; j <= roll; j++){
					if(a == j) success += 1; 	
				}
				
			}
			//System.out.println("Onnistumisia: "+ success);
			//System.out.println("Ratkaisu "+n+ "d l�ytyi yrityksell� " + count);
			//System.out.println("Noppa tulos oli " +a +" "+b+" "+c);
		}
		final long endTime = System.nanoTime();
		System.out.println("Keskiarvona "+toisto +" yrityksest� on " + success/toisto + " onnistumista.");
		long duration = endTime - startTime;
		System.out.println("Aikaa meni " + duration +" nanosekunttia eli "+ duration/1000000 +" millisekunttia." );
		
	}
	
	public static List<Integer> tuplaSuccess(int dtarget, int die) {
		List<Integer> tupla = new ArrayList<Integer>();
		for(int i = dtarget; i <= die; i++ ){
			tupla.add(i);
		}
		return tupla;
	}

}
