import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
//import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Label;
//import java.awt.LayoutManager;
import java.awt.Panel;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
//import java.awt.event.WindowListener;
import java.util.ArrayList;
import java.util.List;


public class ParalGUI extends Frame {
	List<GeneratorParal> listGP;
	Settings set;
	ArrayList<Label> ts;
	ArrayList<Panel> threadpanel;
	Label text;
	
	public ParalGUI(List<GeneratorParal> listGP,Settings set) {
		this.setLayout(new FlowLayout());
		this.listGP = listGP;
		GeneratorParal gp;
		this.set = set;
		threadpanel = new ArrayList<Panel>(set.threads);
		ts = new ArrayList<Label>(set.threads);
		for (int i = 0; i < listGP.size(); i++){
			gp = listGP.get(i);
			if (gp != null){
				Panel uus = new Panel(new GridLayout());
				Label lab1 = new Label(Integer.toString(gp.index));
				Label lab2 = new Label(Long.toString(gp.t));
				lab1.setVisible(true); lab2.setVisible(true);
				ts.add(lab2);
				uus.add(lab1);
				uus.add(lab2);
				threadpanel.add(uus);
			}
			
		}
		this.setMinimumSize(new Dimension(400,200));
		this.setVisible(true);
		this.addWindowListener(new WindowAdapter() {
	           public void windowClosing(WindowEvent e) {
	               e.getWindow().dispose();
	               }
		
		});
		Panel temp;
		for(int i = 0; i < this.threadpanel.size(); i++){
			temp = threadpanel.get(i);
			temp.setVisible(true);
			this.add(temp);
		}
		this.paintAll(getGraphics());
	}
	
	public void update(){
		GeneratorParal gp;
		for(int i = 0; i< this.listGP.size();i++) {
			gp = listGP.get(i);
			ts.get(i).setText(Long.toString(gp.t));
		}
		
		
		
		this.paintAll(getGraphics());
		//this.text.setText(Integer.toString(this.listGP.size()));
	}
	
	public void finalize(long success,long t,long startTime,long endTime){
		
		
		String finalstring = "Yhteens� tuloksia: " + success +"\n";
		
		double tuloss = (double)success/t; 
		finalstring += "Keskiarvona "+t +" yrityksest� on " +tuloss + " onnistumista."+"\n";
		long duration = endTime - startTime;
		finalstring +="Aikaa meni " + duration +" nanosekunttia eli "+ duration/1000000 +" millisekunttia. Eli "+ duration/set.nanoTime1S +" sekunttia. Aka "+ duration/set.nanoTime1S/60 +" minuuttia."+"\n";
		Label tulos = new Label(finalstring);
		this.add(tulos);
		this.update();
	}
	
	
	
	
}
