import java.util.ArrayList;
import java.util.List;
import java.lang.Thread.State;
//import java.util.logging.*;

public class ParalController {
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Settings set = new Settings();
		
		List<GeneratorParal> listGP = new ArrayList<GeneratorParal>(set.threads);
		
		List<Tulos> listT = new ArrayList<Tulos>(set.threads);
		GeneratorParal gp;
		
		Tulos tulos;
		for(int i = 0; i< set.threads; i++){
			tulos = new Tulos(i);
			listT.add(tulos);
			if(set.timed) listGP.add(new GeneratorParalAika(i,set,tulos));
			else listGP.add(new GeneratorParalToisto(i,set, tulos));
		}
		//ParalGUI frame = new ParalGUI(listGP,set);
		final long startTime = System.nanoTime();
		for(int k = 0; k < listGP.size(); k++){
			gp = listGP.get(k);
			gp.start();
			System.out.println("K�ynnistet��n S�ie: " + k);
		}
		//frame.update();
		boolean stillRunning = true;
		long tempTime = startTime;
		long nowTime;
		long timeDif;
		Thread.State temp;
		boolean tsbool;
		while(stillRunning == true){
			//System.out.println("Still here!");

			nowTime = System.nanoTime();
			timeDif = nowTime - tempTime;
			if (timeDif >= set.reportIntervalNano){
				//frame.update();
				System.out.println("Has run for "+ (nowTime - startTime) +" nanoseconds (" + (nowTime - startTime) / set.nanoTime1S + ") s");
				for(int i= 0; i< listGP.size();i++){
					gp = listGP.get(i);
					System.out.print("S�ie "+ i+ " ");
					gp.report();
					if(!set.timed) System.out.println("per    " +set.toisto/set.threads);
					
				}
				tempTime = System.nanoTime();		
			}
			stillRunning = false;
			
			for(int running= 0; running < listGP.size(); running++){
				gp = listGP.get(running);
				temp = gp.getState();
				if(temp == State.TERMINATED){ 
					System.out.println("S�ie "+ running + " " + temp +" at t= "+ gp.t);
					tsbool=false; 
				}else{
					tsbool=true;
				}
				
				stillRunning = stillRunning || tsbool;
			}
			try{
				if (stillRunning) Thread.sleep(1000);
			} catch (InterruptedException e) {
				System.out.println("PANIC!");
				break;
			}
			
		}
		final long endTime = System.nanoTime();
		System.out.println("Laskenta suoritettu. Kootaan tuloksia.");
		//frame.update();
		int success =0;
		long t=0;
		Tulos tul;
		for(int i=0;i < listGP.size();i++){
			gp=listGP.get(i);
			tul = gp.res;
			success += tul.success;
			t+= gp.t;
			//tul.print();
		}
		
		System.out.println("Yhteens� tuloksia: " + success);
		//frame.finalize(success,t,startTime,endTime);
		double tuloss = (double)success/t; 
		System.out.println("Keskiarvona "+t +" yrityksest� on " +tuloss + " onnistumista.");
		long duration = endTime - startTime;
		System.out.println("Aikaa meni " + duration +" nanosekunttia eli "+ duration/1000000 +" millisekunttia. Eli "+ duration/set.nanoTime1S +" sekunttia. Aka "+ duration/set.nanoTime1S/60 +" minuuttia.");
		
	}
	

}
