import java.util.Random;

public class NoppaTesti {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		int toisto = 100000000;
		long summa = 0;
		final long startTime = System.nanoTime();
		for(int t = 0;t<toisto;t++){
			int n = 6;
			long count = 0;
			Random rand = new Random();
			int a=0;int b=1; int c=2;
			while (a!=b || b!=c) {
				a = rand.nextInt(n) +1;
				b = rand.nextInt(n) +1;
				c = rand.nextInt(n) +1;
				count += 1;
			}
			//System.out.println("Ratkaisu "+n+ "d l�ytyi yrityksell� " + count);
			//System.out.println("Noppa tulos oli " +a +" "+b+" "+c);
			summa +=count;
		}
		final long endTime = System.nanoTime();
		System.out.println("Keskiarvona "+toisto +" yrityksest� on " + summa/toisto);
		long duration = endTime - startTime;
		System.out.println("Aikaa meni " + duration +" nanosekunttia eli "+ duration/1000000 +" millisekunttia." );
		
	}
}