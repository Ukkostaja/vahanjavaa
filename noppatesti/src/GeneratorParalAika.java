
public class GeneratorParalAika extends GeneratorParal {

	public GeneratorParalAika(int index, Settings set, Tulos result){
		super(index,set,result);
	}
	
	
	public void run(){
		this.startTime= System.nanoTime();
		while(System.nanoTime() - this.startTime < this.set.runningTime){
			t++;
			this.roll();
				
		}
	}
		
}
