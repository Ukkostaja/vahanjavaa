
public class Tulos {
	public final int index;
	public long toisto;
	public long success;
	public long startTime;
	
	public Tulos(int index) {
		this.index = index;
		this.success=0;
	}
	
	public void print() {
		System.out.println("Tulosolio n." + this.index + " raportoi "+ this.success + " onnistumisesta.");
	}
	
}
