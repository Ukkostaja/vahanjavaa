
public class Settings {
	public
		final boolean timed;
		final boolean explodingTens;
		final int threads;
		final int toisto;
		final int die;
		final int roll;
		final int target;
		final int dtarget;
		final long reportIntervalNano;
		final long runningTime;
		final long nanoTime1S = 1000000000L; // on 1 sec.
		final long deep;
		
		
	public Settings(){
		this.timed = true; //if true makes Aika threads if false makes Toisto Threads.
		this.threads = 2;
		this.toisto = 10000000;
		this.die = 10;
		this.roll = 10;
		this.target = 8;
		this.dtarget= 10;
		this.explodingTens = true;
		this.deep = 10000;
		this.reportIntervalNano = 3* nanoTime1S; // on 3 s.
		this.runningTime = 15*nanoTime1S;
	}
}
