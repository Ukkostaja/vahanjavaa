package pn2011;
import java.util.Random;

public class paa {
	static int first = 32;
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		/*
		 Valot valot = new Valot();
		 
		valot.tee(32, 100, 100, 100);
		*/
		//lightup();
		//ohjelma();
		//darkdown();
		//red();
		
	}
	
	public static void lightup() {
		Valot valot = new Valot();
		for(int i = 0; i< 14; i++){
			valot.tee(first+i, 255,255, 255);
		}
	}
	
	public static void red(){
		Valot valot = new Valot();
		for(int i = 0; i< 14; i++){
			valot.tee(first+i, 255, 0, 0);
		}
	}
	
	public static void darkdown() {
		Valot valot = new Valot();
		for(int j = 255; j > 0; j= j-5){
			wait(2);
			for(int i = 0; i < 14;i++) {
				valot.tee(first+i, j, j, j);
			}
		}
	}
	
	public static void ohjelma() {
		Random rdom = new Random();
		
		Valot valot = new Valot();
		for(int j = 0; j < 256; j= j+5){
			wait(2);
			for(int i = 0; i < 14;i++) {
				valot.tee(secvencer(first+i,(first+i) % 3, rdom.nextInt(256)));
			}
		}
		for(int k = 0; k < 14;k++) {
			valot.tee(secvencer(k+first, k % 3, rdom.nextInt(256)));
		}
		wait(200);
		valot.close();
	}
	
	public static int[] secvencer(int id, int sec, int intensity) {
		int[] tulos = {id, 0,0,0};
		tulos[sec+1] = intensity;
		return tulos;
	}
	
	public static void wait(int n){
        long t0,t1;
        t0=System.currentTimeMillis();
        do{
            t1=System.currentTimeMillis();
        }
        while (t1-t0<n*100);
	}
}
