import java.awt.*;
import java.awt.event.*;



	public class EarMouse implements MouseListener{
	    Component c;
		
		public void mousePressed(MouseEvent e) {
	    	
	    	this.c = e.getComponent();
	    	
	        eventOutput("Mouse pressed (# of clicks: "
	                + e.getClickCount() + ")", e);
	    }
	     
	    public void mouseReleased(MouseEvent e) {
	        eventOutput("Mouse released (# of clicks: "
	                + e.getClickCount() + ")", e);
	    }
	     
	    public void mouseEntered(MouseEvent e) {
	        eventOutput("Mouse entered", e);
	    }
	     
	    public void mouseExited(MouseEvent e) {
	        eventOutput("Mouse exited", e);
	    }
	     
	    public void mouseClicked(MouseEvent e) {
	        eventOutput("Mouse clicked (# of clicks: "
	                + e.getClickCount() + ")", e);
	    }

	    public void eventOutput(String s, MouseEvent e){
	    	System.out.println(s);
	    }
	}

