import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Scanner;


public class EuroChecksum {

	ArrayList<String> countryCode = new ArrayList<>(26);
	int[] checkCode = new int[26];
	ArrayList<String> country = new ArrayList<>(26);
	
	public void EuroChecksum() {
		FileReader fr = null;
		String line;
		String[] parts;
		int count = 0;
		try {
			fr = new FileReader("EuroContryCodes.csv");
			BufferedReader br = new BufferedReader(fr);
			while(br.ready()) {
				count++;
				line = br.readLine();
				parts = line.split(",");
				if(parts.length == 3) {
					countryCode.add(count, parts[0]);
					checkCode[count] = Integer.parseInt(parts[1]);
					country.add(count, parts[2]);
				}
			}
			
		} catch (Exception e) {
			// TODO: handle exception
		} 
	}
	
	public String character(char ch) {
		int result =0;
		result = Character.getNumericValue(ch) + 55;
		return (new Integer(result)).toString();
	}
	
	public int combine(int luku) {
		int b = luku % 10;
		int a = luku / 10;
		return a+b;
	}
	
	public int checksum(String ch) {
		int index = countryCode.indexOf(ch);
		return checkCode[index];
		
	}
	
	public boolean verify() {
		return true;
	}
	
	public static void write(String line) {
		FileWriter fw = null;
		try {
			fw = new FileWriter("EuroSerials.txt", true);
			fw.write(line+"\n");
		} catch (Exception e) {
			
		} finally {
			try {
				fw.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	public void loop(){
		System.out.println("Please enter serial number.");
		Scanner sc = new Scanner(System.in);
		int total;
		String line = " "; String chara;
		while(line != ""){
			line = sc.next();
			if(line.length() != 12){
				sc.close();
				System.out.println("Breaking.");
				break;
			}
			line.toUpperCase();
			String addition =character(line.charAt(0));
			String replaced = line.replaceAll("[A-Z]", addition);
			System.out.print(line + " " + replaced +" ");
			long value = Long.parseLong(replaced);
			write(line);
			if(value % 9 == 0) {
				System.out.println("Genuine");
			} else {
				System.out.println("Fake");
			}
		} 
		sc.close();
	}
	
	public static void main(String[] args) {
		EuroChecksum ec = new EuroChecksum();
		ec.loop();
		// X74957079746
	}

}
