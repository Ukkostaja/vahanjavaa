import java.awt.Canvas;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.event.*;
import java.util.*;
import java.util.Timer;

import javax.swing.*;

public class Bugger extends Canvas {
    private static final int WIDTH =1024;
    private static final int HEIGHT = 800;
    private static final Random random = new Random();

    
    JavaReminder jr;
    ArrayList<SBug> bugs;
    Graphics g;
    boolean once;
    int sx,sy,fx,fy;
    Bugger(){
    	super();
    	once = true;
    	this.setMouseListener();
    	bugs = new ArrayList<>();
    	for(int i=0;i < 10;i++){
    		bugs.add(new SBug(WIDTH/2, HEIGHT/2, Color.blue));
    	}
    	this.jr = new JavaReminder(this);
    }
    

    public void setMouseListener() {
        MouseListener internalMouseListener = new MouseAdapter() {
            public void mousePressed(MouseEvent mouseEvent) {
              //System.out.println("Pressed");
             BuggerPaintD(mouseEvent.getX(),mouseEvent.getY());
             
            }
            
            public void mouseReleased(MouseEvent mouseEvent) {
                //System.out.println("Pressed");
               BuggerPaintU(mouseEvent.getX(),mouseEvent.getY());
               
              }
          };
    	this.addMouseListener(internalMouseListener);
    }
    
    @Override
    public void paint(Graphics g) {
        //super.paint(g);
    	
    		g.setColor(Color.gray);
            this.setBackground(Color.black);
            g.fillOval(WIDTH/4, HEIGHT/4, 500, 500);
           // System.out.println(g.getColor().toString());
            //System.out.println(x+","+y+" ");
            
            
    }
    
    @Override
    public void update(Graphics g) {
    	//System.out.println();
    	g.setColor(Color.pink);
    	g.drawLine(this.sx, this.sy, this.fx, this.fy);
    	for(int i = 0; i < bugs.size();i++){
    		bugs.get(i).antipaint(g);
    	}
    	
    	for(int i = 0; i < bugs.size();i++){
    		bugs.get(i).update();
    	}
    	for(int i = 0; i < bugs.size();i++){
    		bugs.get(i).putpaint(g);
    	}
    }
    
    public void BuggerPaintU(int x, int y) {
    	this.fx =x;this.fy = y;
    	
    	this.repaint();
    }
    
    
    public void BuggerPaintD(int x, int y) {
    	this.sx =x;this.sy = y;
    	//System.out.print(x+","+y+" ");

    }

    private Color randomColor() {
        return new Color(random.nextInt(256), random.nextInt(256), random.nextInt(256));
    }

    public static void main(String[] args) {


        JFrame frame = new JFrame();
        Bugger bugger = new Bugger();

        frame.setSize(WIDTH, HEIGHT);
        frame.add(bugger);

        frame.setVisible(true);
    }
    public class JavaReminder {
        Timer timer;
        Bugger myBugger;

        public JavaReminder(Bugger bugger) {
            timer = new Timer();  //At this line a new Thread will be created
            timer.schedule(new RemindTask(),0, 500); //delay in milliseconds
            this.myBugger = bugger;
        }

        class RemindTask extends TimerTask {

            @Override
            public void run() {
                myBugger.repaint();
            }
        }
    };
    
}

