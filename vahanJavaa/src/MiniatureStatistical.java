import java.util.ArrayList;
import java.util.Random;

public class MiniatureStatistical {
	ArrayList<MiniatureCollection> somanytimes;
	int howmany = 1000000;
	
	
	public MiniatureStatistical() {
		somanytimes = new ArrayList<>();
		for(int i=0;i< howmany;i++) {
			somanytimes.add(new MiniatureCollection());
		}
	}
	
	public void Go() {
		for(int i = 0; i< somanytimes.size();i++) {
			somanytimes.get(i).Do();
		}
	}
	
	private int TotalUp() {
		int total = 0;
		for (MiniatureCollection collection : somanytimes) {
			total+=collection.tries;
		}
		return total;
	}
	
	private MiniatureCollection GetRandomOne(){
		Random rdom = new Random();
		return somanytimes.get(rdom.nextInt(howmany));
	}
	
	public void PrintStatistics() {
		int total = TotalUp();
		int avarage = total/howmany;
		System.out.println("Avarage number of boosters: "+avarage);
		System.out.println("Random example collection:");
		GetRandomOne().Print();
		
		
	}
	
	public static void main(String[] args) {
		MiniatureStatistical MS = new MiniatureStatistical();
		MS.Go();
		MS.PrintStatistics();


	}

}
