
public class GameItem {
	String Name;
	float Time;
	
	GameItem(String name, float time) {
		Name = name;
		Time = time;
	}
	
	public float getTime() {
		return Time;
	}
	
	public String toString() {
		String line = "I am "+Name + ". You have played me for "+Time+" hrs.";
		return line;
	}
	
}
