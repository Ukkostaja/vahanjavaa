import java.awt.Color;
import java.awt.Graphics;
import java.util.Random;
public class SBug {
	int x,y,size;
	Color type;
	Random rand;
	
	public SBug(int x, int y, Color type) {
		this.size = 4;
		this.x= x;
		this.y = y;
		this.type = type;
		this.rand = new Random();
	}
	
	public void antipaint(Graphics g){
		g.setColor(Color.gray);
		g.fillRect(this.x, this.y, size, size);
	}
	
	public void putpaint(Graphics g){

		g.setColor(type);
		g.fillRect(x, y, size, size);
	}
	
	public void paint(Graphics g){
		this.antipaint(g);
		this.update();
		this.putpaint(g);
	}
	
	public void update(){
		x+=rand.nextFloat()*4-2;
		y+=rand.nextFloat()*4-2;
	}
}
