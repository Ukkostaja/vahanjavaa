import java.util.Random;

public class MiniatureCollection {
	
	int[] minis;
	public long tries;
	Random rdom;
	
	public MiniatureCollection() {
		minis = new int[44];
		tries = 0;
		rdom = new Random();
	}

	private int GetOne() {
		return rdom.nextInt(minis.length);
	}
	
	private void AddOne() {
		int index = GetOne();
		minis[index]++;
	}
	
	private void Add(int howmany) {
		for(int i = 0;i< howmany;i++) {
			AddOne();
		}
	}
	
	public boolean AreWeDone() {
		for(int i=0; i< minis.length;i++){
			if(minis[i] == 0) return false;
		}
		return true;
	}
	
	public void Do() {
		while(!AreWeDone() ) {
			tries++;
			Add(4);
		}
	}
	
	public void Print() {
		System.out.println("Tries: "+ tries);
		int total_minies = 0;
		for(int i =0;i < minis.length;i++) {
			System.out.println("mini nro." +(i+1)+ " got "+minis[i]+".");
			total_minies+= minis[i];
		}
		System.out.println("Total number of minis: "+total_minies);
	}
	
	public static void main(String[] args) {
		System.out.println("Starting...");
		MiniatureCollection MC = new MiniatureCollection();
		System.out.println("Object Created.");
		MC.Do();
		System.out.println("Work done. Printing...");
		MC.Print();
		System.out.println("Done.");
		
	}

	
	
}
