import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.*;


public class GameSuggester extends JFrame  {
	
	ArrayList<GameItem> all = new ArrayList<GameItem>(100);
	ArrayList<GameItem> zeroTime = new ArrayList<>(100);
	int index;
	String textField;
	JTextField jtf;
	JTextArea jl;
	String id;
	JSlider js;
	
	void readURL(String urlString) {
		try {
			URL url = new URL(urlString);
			BufferedReader br = new BufferedReader(
			        new InputStreamReader(
			        		url.openStream()
			        )
			);
			parseFromNet(br);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	String parseValue(String line,String key) {
		int indexStart = line.indexOf(key, index)+key.length();
		if (indexStart < index) return null;
		int indexEnd = line.indexOf("\"",indexStart);
		//System.out.println(indexStart+ " " + indexEnd);
		String value= line.substring(indexStart, indexEnd);
		//System.out.println(value);
		index = indexEnd;
		return value;
	
	}
	
	void parseFromNet(BufferedReader br) {
		String line;
		
		try {
			while( (line = br.readLine()) != null) {
				//System.out.println("*STARTLINE*"+line + "*ENDLINE*");
				//System.out.println(line);
				if(line.contains("var rgGames =")) {
					//System.out.println(line);
					index = 0;
					while(true) {
						System.out.print(".");
						String nameKey = "\"name\":\"";
						//System.out.println(nameKey);
						String name = parseValue(line, nameKey);
						if(name == null)  {
							System.out.println();
							MakeZeroTime();
							return;
						}
						String timeS = parseValue(line, "\"hours_forever\":\"");
						float time;
						//System.out.println(name + " "+timeS);
						if(timeS == null) {
							time = 0;
						} else {
							time = Float.parseFloat(timeS);
						}
						GameItem gi = new GameItem(name, time);
						all.add(gi);
						
					}
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	void parseLocalFile(BufferedReader br) {
		try {
			String line;
			while( (line = br.readLine()) != null) {
				//System.out.println("*STARTLINE*"+line + "*ENDLINE*");
				//System.out.println(line);
				if(line.contains("<div class=\"gameListRowItemName \">")) {
					//System.out.println("Got GAME");
					int end = line.indexOf("</div>"); //figure out the name
					String name = line.substring(35, end);
					
					line = br.readLine(); // time played is on the next line
					end = line.indexOf(" ");
					float time;
					if (end == -1) {
						time = 0;
					} else {
						//System.out.println(line);
						String subline = line.substring(5	, end);
						//System.out.println(subline);
						time = Float.parseFloat(subline);
					}
					GameItem uusi = new GameItem(name, time);
					System.out.println(uusi.toString());
					all.add(uusi);
				}
			} 
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			MakeZeroTime();
		}
	
	}
	
	void ReadFile(String filename) {
			FileReader f;
			try {
				f = new FileReader(filename);
				BufferedReader br = new BufferedReader(f);
				parseLocalFile(br);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		
			
	}
	
	String PickRandom(ArrayList<GameItem> relevant) {
		Random rdom = new Random();
		int index = rdom.nextInt(relevant.size());
		return relevant.get(index).toString();
	}
	
	public String PickRandomAll() {
		return PickRandom(all);
	}
	
	void MakeZeroTime() {
		for (GameItem gameItem : all) {
			if(gameItem.getTime() == 0 ){
				zeroTime.add(gameItem);
			}
		}
		
	}
	
	void TextGeneration() {
		textField = "From all:\n";
		textField = textField+ this.PickRandomAll() + "\n";
		textField = textField +"From zero time played:\n";
		textField = textField +this.NZeroTimes(js.getValue());
	}
	
	public String NZeroTimes(int n) {
		String text = "";
		for(int i = 0; i<n;i++){
			text = text + PickRandomWithZeroTime() + "\n";
		}
		return text;
	}
	
	public String PickRandomWithZeroTime() {
		return PickRandom(zeroTime);
	}
	
	void GetListButton() {
		all.clear();
		readURL("http://steamcommunity.com/id/"+jtf.getText() +"/games?tab=all");	
		
		
	}
	
	void SuggestButton(){
    	if(all.size() == 0) {
    		GetListButton();
    	}
    	if(!id.contentEquals(jtf.getText())) {
    		GetListButton();
    		id = jtf.getText();
    	}
    	TextGeneration();
        jl.setText(textField);
	}
	
	public void OnGUI() {
		setTitle("Steam: What to Play");
	    setSize(600,350);
	    //setLocationRelativeTo(null);
	    setDefaultCloseOperation(EXIT_ON_CLOSE);
	    js = new JSlider(JSlider.HORIZONTAL,1,60,10);
	    
	    id = "Ukkostaja";
	    jtf = new JTextField(id);
	    jtf.setPreferredSize(new Dimension(100,25));
		JButton jbGET = new JButton("Get List");
	    jl = new JTextArea();
		jbGET.addActionListener(new ActionListener() {
		    @Override
		    public void actionPerformed(ActionEvent event) {
		        GetListButton();
		    }
		});
		JButton jbArvo = new JButton("Suggest");
		jbArvo.addActionListener(new ActionListener() {
		    @Override
		    public void actionPerformed(ActionEvent event) {
		    	SuggestButton();
		    }
		});

	    JPanel jp = new JPanel();
	    
	    jp.add(jtf);
	    jp.add(jbGET);
	    jp.add(jbArvo);
	    jp.add(js);
	    jp.add(jl);
	    add(jp);
	}
	
	public static void main(String[] args) {
		System.out.print("Generating");
		GameSuggester gs = new GameSuggester();
		gs.OnGUI();
		gs.setVisible(true);
		/*
		if(args.length == 1) {
			//gs.ReadFile(args[0]);
			gs.readURL("http://steamcommunity.com/id/Ukkostaja/games?tab=all");

		}
		*/
	}

}
