import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;


public class MM1referenceParser {
	private ArrayList<String> keywords;
	private ArrayList<Integer> pages;
	
	public void list(){
		for(int i = 0; i< keywords.size(); i++){
			System.out.println(keywords.get(i) +"="+ pages.get(i).toString());
		}
	}
	
	public int hasPage(String value) {
		for(int i= 0 ; i < keywords.size(); i++){
			if(keywords.get(i).equalsIgnoreCase(value)){
				return pages.get(i).intValue();
			}
		}
		return -1;
	}
	
	
	
	public void crossPreference(MonsterDen md){
		// TO DO
		int value;
		for(int i = 0; i< md.size();i++){
			Monster temp = md.get(i);
			//Check combined tag first
			value =this.hasPage(temp.getTagsAsString());
			
			//check individual tags
			if ( value == -1){
				ArrayList<String> tags = temp.getTags();
				for(int j = 0; j < tags.size(); j++){
					if( value== -1) {
						value =this.hasPage(tags.get(j));
					}
				}
			}
			//check combined name
			String mname = temp.getName();
			if( value== -1){
				value = this.hasPage(mname);
				temp.setPage(value);
			}
			
			
			//check part names
			if ( value == -1){
				String[] pname = mname.split(" ");
				//System.out.println(pname);
				for(int j = 0; j < pname.length; j++){
					//System.out.println(pname[j]);
					if ( value == -1){
						value = this.hasPage(pname[j].trim());
					}
				}
			}
			
			temp.setPage(value);
		}
	}
	
	public MM1referenceParser(){
		int ln = 0;
		int page;
		keywords = new ArrayList<String>(160);
		pages = new ArrayList<Integer>(160);
		String line;
		ReadFile rf = null;
		String[] halves;
		try {
			rf = new ReadFile("mm1 pages.txt");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			while( (line= rf.readLine()) != null){
				
				halves = line.split(",");
				if(halves.length != 2) {
					System.out.println(line);
				} else {
					keywords.add(ln, halves[0].trim());
					String ps = halves[1].trim();
					pages.add(ln, Integer.decode(ps));
				}
				
				ln++;
			}
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
}
