import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;

import javax.swing.*;
import javax.swing.table.*;

public class MonsterDen extends AbstractTableModel {
	private ArrayList<Monster> mdb;
	private final String unitefile = "mmu";
	private int progress;
	//private TableModelListener l;
    private String[] columnNames = {
    		"Name",
            "Level",
            "Role",
            "Leader",
            "Special",
            "Book",
            "Page",
            "Tags"
    };
	
	
	MonsterDen(){
		this.mdb = new ArrayList<Monster>(1300);
		//this.addTableModelListener(new MyTableEventListener(this));

	}
	
	public Monster get(int i){
		return this.mdb.get(i);
	}
	
	public int size(){
		return this.mdb.size();
	}
	
	public int progress(){
		return this.progress();
	}
	
	public void setAllPrimarySplitter(String newValue){
		for(int i = 0;i< mdb.size();i++){
			mdb.get(i).setPrimarySplitter(newValue);
		}
	}
	
	public void setAllSecondarySplitter(String newValue){
		for(int i = 0;i< mdb.size();i++){
			mdb.get(i).setSecondarySplitter(newValue);
		}
	}
	
	public void readAll() {
		
		this.readCurrent();
		String[] books = {"mm1", "mm2","mm3"};
		
		for(int i = 0; i< books.length; i++) {
			readFile(books[i]);
		}
		
		
	}
	
	public void readFile(String name) {
		progress = 0;
		boolean unite;
		if(name.equalsIgnoreCase(this.unitefile)) {
			unite = true;
		} else {
			unite = false;
		}
		String line;
		ReadFile rf = null;
		try {
			rf = new ReadFile(name+".csv");
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			while( (line = rf.readLine()) != null){
				progress++;
				if(unite){
					try {
						this.mdb.add(new Monster(line));
					} catch (WrongAmountOfValues e) {
						System.out.println("Wrong amount of Values error: "+ e.numberOfValues + " for line: "+line);
					}
					
				} else {
					this.mdb.add(new Monster(line, name));
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void readCurrent(){
		readFile(this.unitefile);

	}
	
	public boolean validate(){
		boolean valid = true;
		String rsc;
		for(int i= 0; i< this.mdb.size(); i++){
			Monster test = this.mdb.get(i);
			if (test!= null){
				rsc = test.roleSanityCheck();
				
				if(!rsc.equalsIgnoreCase(" sane ")){
					System.out.println("RoleError with "+ rsc +" in "+ test.status());					
					valid = false;
				} else {
					//System.out.println("***OK*** " + test.status());
				}
			}
		}
		return valid;
	}
	
	public void writeCurrent() {
		if(this.validate()){
			this.writeFile();
		}
	}
	
	public void writeFile(){
		progress = 0;
		try {
			WriteFile wf = null;
			wf = new WriteFile(this.unitefile+".csv");
			
			for(int i= 0;i< this.mdb.size();i++) {
		
				wf.write(mdb.get(i).saveString());
				wf.newLine();
				progress++;
			}
			wf.close();
		} catch (IOException e) {
		e.printStackTrace();
		}
		
	}



	@Override
	public int getColumnCount() {
		// TODO Auto-generated method stub
		return 8;
	}

	 public String getColumnName(int col) {
         return columnNames[col];
     }
	
	  public Class getColumnClass(int c) {
          return getValueAt(0, c).getClass();
      }
	  
	  public boolean isCellEditable(int row, int col) {

		  return true;
      }
	  
      public void setValueAt(Object value, int row, int col) {
          this.mdb.get(row).setValueByColumn(col, value);
          fireTableCellUpdated(row, col);
      }
	 
	@Override
	public int getRowCount() {
		// TODO Auto-generated method stub
		return mdb.size();
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		return this.mdb.get(rowIndex).getValueByColumn(columnIndex);
	}
}
