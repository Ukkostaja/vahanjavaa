import java.util.ArrayList;


public class Monster {
	private String name;
	private int lvl;
	private String role;
	private boolean leader;
	private String special = "Normal"; // 0 = minion,1 = normal, 2 = elite, 3 = solo;
	private String book;
	private int page;
	private ArrayList<String> tags;
	public static final String kelpoRole = "Skirmisher Brute Soldier Lurker Controller Artillery";
	public static final String kelpoSpecial = "Minion Normal Elite Solo";
	
	private String primarySplitter = ";";
	private String secondarySplitter = " ";
	
	public String saveString(){
		String savestring = this.name + primarySplitter + String.valueOf(this.lvl) + primarySplitter + this.role + primarySplitter + String.valueOf(this.leader) + primarySplitter +
		this.special + primarySplitter+ this.book +primarySplitter +this.page + primarySplitter;
		for(int i = 0; i < tags.size(); i++){
			savestring+=tags.get(i) + secondarySplitter;
		}
		//savestring+="\n";
		return savestring;
	}
	
	public void setPage(int page){
		this.page = page;
	}
	
	public int getPage(){
		return this.page;
	}
	
	private String specialConvert(int i){
		switch (i) {
		case 0:
			return "Minion";
		case 2:
			return "Elite";
					
		case 3:
			return "Solo";
			
		default:
			return "Normal";
			
		}
	}
	
	
	public Monster(String savestring) throws WrongAmountOfValues{
		this.tags = new ArrayList<String>();
		String[] values = savestring.split(primarySplitter);
		//System.out.println(values.length);
		if((values.length < 7) || (values.length > 8)){
			WrongAmountOfValues error = new WrongAmountOfValues(values.length);
			throw error;
		}
		this.name = values[0];
		this.lvl = Integer.parseInt(values[1]);
		this.role = values[2];
		this.leader = Boolean.parseBoolean(values[3]);
		this.special = values[4];
		this.book = values[5];
		this.page = Integer.parseInt(values[6]);
		if(values.length == 8){
			this.tags = parseTags(values[7]);
		}
		
	}
	
	public Monster(String rivi, String book) {
		this.tags = new ArrayList<String>();
		
		this.book = book;
		if(book.contentEquals("mm1")){
			parsemm1(rivi);
		} else {
			parse(rivi);
		}
	}
	
	private ArrayList<String> parseTags(String srivi){
		ArrayList<String> tagus = new ArrayList<String>();
		String[] tulos = srivi.split(secondarySplitter);
		for(int i =0; i < tulos.length; i++) {
			String tag = tulos[i];
			if(tag != null) {
				tagus.add(tag);
			}
		}
		
		return tagus;
	}
	
	private void parsemm1(String rivi){
		int rio = rivi.indexOf("(");
		int firststop = rivi.indexOf(primarySplitter);
		if(firststop== -1){
			System.out.println(rivi);
		}
		String nimi; 
		if( (rio != -1) && (rio < firststop)) {
			//lähetetään tagit tutkittavaksi
			this.tags = parseTags(rivi.substring(rio+1, rivi.indexOf(")") ));
			nimi = rivi.substring(0, rio );
			
			
		} else {
			nimi = rivi.substring(0,firststop);
		}
		
		this.name = nimi.trim();
		
		
		int le = rivi.indexOf(secondarySplitter, firststop);
		//System.out.println(le);
		String slvl = rivi.substring(firststop+1,le);
		try {
			this.lvl= Integer.parseInt(slvl);
		} catch (NumberFormatException e) {
			System.out.println(rivi+ " in book" + this.book);
			e.printStackTrace();
		}
		int lio = rivi.lastIndexOf(primarySplitter);
		String trole;
		if (lio == firststop){
			trole = rivi.substring(le);
		} else {
			trole = rivi.substring(le	, lio );
		}
		this.role= new String();// must initiaze role;
		
		this.parseRole(trole);
	}
	
	private void parseRest(String rivi){
		int lio = rivi.lastIndexOf(primarySplitter);
		//System.out.println(lio + secondarySplitter +rivi.length());
		String ps = rivi.substring( (lio+1));
		//System.out.println(rivi);
		this.page = Integer.parseInt(ps.trim());
	}
	
	private void parse(String rivi){
		parsemm1(rivi);
		this.parseRest(rivi);
		
	}
	
	/***
	 * performs sanity check on the current role. Returns " Sane " if sane. returns current role if not.
	 * @return
	 */
	public String roleSanityCheck(){
		String kelpol = this.kelpoRole.toLowerCase();
		String sanatl = this.role.toLowerCase();
		String[] sanat = sanatl.split(secondarySplitter);
		for(int i = 0; i< sanat.length; i++){
			String sana = sanat[i];
			if (sana!= null){
				if(!kelpol.contains(sana)){
					return sana;
				}
			}
		}
		
		return " Sane ";
	}
	
	public Object ColumnMani(int column, boolean write, Object obj){
		//System.out.println(obj);
		Object tulos = obj;
		switch (column) {
		case 0:
			if(write){
				this.name = (String) tulos;
			} else {
			tulos = this.name;
			}
			break;
		case 1:
			if(write){
				this.lvl =((Integer)tulos).intValue();
			} else {
			tulos = this.lvl;
			}
			break;
		case 2:
			if(write){
				this.role = (String) tulos;
			} else {
			
			tulos = this.role;
			}
			break;
		case 3:
			if(write){
				this.leader = ((Boolean)tulos).booleanValue();
			} else {
		
			tulos = this.leader;
			}
			break;
		case 4:
			if(write){
				this.special = (String) tulos;
			} else {
			tulos = this.special;
			}
			break;
		case 5:
			if(write){
				this.book = (String) tulos;
			} else {
			tulos = this.book;
			}
		
			break;
		case 6:
			if(write){
				this.page = ((Integer)tulos).intValue();
			} else {
			tulos = this.page;
			}
			break;
		case 7:
			if(write){
				this.tags = (ArrayList<String>) tulos;
			} else {
			tulos = this.tags;
			}
			break;
		
		default:
			tulos = null;
		}
		return tulos;
	}
	
	public void setValueByColumn(int column,Object obj){
		//System.out.println(obj);
		this.ColumnMani(column, true, obj);
	}
	
	public Object getValueByColumn(int column){
		return this.ColumnMani(column, false, null);
		
	}
	
	public void setPrimarySplitter(String newValue){
		this.primarySplitter = newValue;
	}
	
	public void setSecondarySplitter(String newValue){
		this.secondarySplitter = newValue;
	}
	
	private void parseRole(String rivi) {
		String[] sanat = rivi.split(secondarySplitter);
		
		for(int i= 0;i < sanat.length;i++){
			String sana = sanat[i];
			boolean add= true;
			if(sana.equalsIgnoreCase("minion")){
				this.special = "Minion";
				add = false;
			}
			if(sana.equalsIgnoreCase("elite")) {
				this.special = "Elite";
				add = false;
			}
			if(sana.equalsIgnoreCase("solo")){
				this.special = "Solo";
				add = false;
			}
			if(sana.equalsIgnoreCase("(L)")){
				this.leader=true;
				add= false;
			}
			if(add){
				this.role += sana +secondarySplitter;
			}

		}
		this.role = this.role.trim();
	}
	
	public ArrayList<String> getTags(){
		return this.tags;
	}
	
	public String getName(){
		return this.name;
	}
	
	public ArrayList<String> getNameList(){
		String[] pn = this.name.split(" ");
		ArrayList<String> tulos= new ArrayList<String>(pn.length);
		for(int i = 0;i < pn.length; i++){
			tulos.add(pn[i]);
		}
		return tulos;
	}
	
	public String getTagsAsString(){
		String tulos = "";
		for(int i=0;i< tags.size();i++){
			tulos+=tags.get(i)+" ";
		}
		return tulos.trim();
	}
	
	public String status(){
		String tulos = "My name is "+name+ ". I'm level "+lvl+" and my role is "+role+primarySplitter;
		if(tags.size()>0){
			tulos+= " My people are ";
			for(int i = 0; i< tags.size(); i++){
				tulos+= tags.get(i)+", ";
			}
			tulos+=primarySplitter;
		}
		if(leader){
			tulos+= " I am the leader of my people.";
		}
		
		tulos+= " I'm "+special +" of my kind.";
		
		
		tulos+=" You should find me in "+book +" page "+page;
		return tulos;
	}
	

}
