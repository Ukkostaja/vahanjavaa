
public class WrongAmountOfValues extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7296818766584981929L;
	public int numberOfValues;
	
	public WrongAmountOfValues(int numberOfValues){
		this.numberOfValues = numberOfValues;
	}

}
