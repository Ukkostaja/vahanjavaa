import java.awt.Dimension;

import javax.swing.*;
import javax.swing.table.*;


public class Gui extends JFrame {
	MonsterDen md;
	
	public Gui(){
	}
	
	public Gui(MonsterDen md) {
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.addWindowListener(new MyWindowListener());
		this.setMinimumSize(new Dimension(200, 100));
		this.setPreferredSize(new Dimension(1000, 800));
		this.setTitle("Monster Den");
		this.md = md;
		//this.setLayout(manager);
		TableModel myData = md;
	    JTable table = new JTable(myData);
	    table.setAutoCreateRowSorter(true);
	    TableColumn column = null;
	    for (int i = 0; i < table.getColumnCount(); i++) {
	    	column = table.getColumnModel().getColumn(i);
	    	switch (i) {
			case 0:
				column.setPreferredWidth(150); //first column is bigger
				break;
			case 3:
				column.setPreferredWidth(25);
				break;
			default:
				column.setPreferredWidth(50);
				break;
			}
	        
	    }
	    this.setUpComboBoxColumns(table, table.getColumnModel().getColumn(2),table.getColumnModel().getColumn(4),  
	    		Monster.kelpoRole.split(" "), Monster.kelpoSpecial.split(" "));
	    table.setRowHeight(20);
	    
	    
		JScrollPane scrollPane = new JScrollPane(table);
		table.setFillsViewportHeight(true);
		this.add(scrollPane);
		this.pack();
		this.setVisible(true);
		
	}
	
	
    public void setUpComboBoxColumns(JTable table,
            TableColumn roleColumn,TableColumn specialColumn, String[] roles, String[] specials) {
		//Set up the editor for the sport cells.
		JComboBox rcomboBox = new JComboBox();
		for(int i = 0; i < roles.length; i++){
			rcomboBox.addItem(roles[i]);
		}
		rcomboBox.addItem("");
		
		JComboBox scomboBox = new JComboBox();
		for(int i = 0; i < specials.length; i++){
			scomboBox.addItem(specials[i]);
		}
		
		roleColumn.setCellEditor(new DefaultCellEditor(rcomboBox));
		specialColumn.setCellEditor(new DefaultCellEditor(scomboBox));
		
		DefaultTableCellRenderer renderer =
		new DefaultTableCellRenderer();
		roleColumn.setCellRenderer(renderer);
		specialColumn.setCellRenderer(renderer);
		
	}
    
    
    
	public void save(){
		this.md.writeCurrent();
	}

}
