
public class Tulokset {
	int miinus;
	int plus;
	int nolla;
	int npj,npe,pjd,ped,raising;
	
	Tulokset(int miinus, int plus, int nolla ){
		this(miinus,plus,nolla,0,0,0,0,0);
	}
	Tulokset(int miinus, int plus, int nolla, int npj,int pjd, int npe, int ped){
		this(miinus, plus, nolla, npj, pjd, npe, ped, 0);
	}
	
	Tulokset(int miinus, int plus, int nolla, int npj,int pjd, int npe, int ped,int raising){
		this.miinus = miinus;
		this.plus = plus;
		this.nolla = nolla;
		this.npj = npj;
		this.npe = npe;
		this.pjd = pjd;
		this.ped = ped;
		this.raising = raising;
	}
	
	public static String print(int i){
		return String.valueOf(i).substring(0, 5);
	}
	
	public void printblock(){
		double summa = miinus+plus+nolla;
		double mpros = miinus*100.0/(summa);
		double ppros = plus*100.0/(summa);
		double npros = nolla*100.0/(summa);
		System.out.print(Tulokset.print(mpros)+ " ");
		System.out.print(Tulokset.print(ppros) + " ");
		System.out.print(Tulokset.print(npros)+ " | ");
	}
	
	public static String print(double i){
		String vali = String.valueOf(i);
		if (vali.length() >4) {
			return vali.substring(0, 4);
		} else {
			if (vali.length() == 4) {
				return vali;
			}
			if (vali.length() == 3) {
				return " "+vali;
			}
			return "*"+vali+"*";
		}
		
		
	}
	public void humanPrint(){
		double summa = miinus+plus+nolla;
		double mpros = miinus*100.0/(summa);
		double ppros = plus*100.0/(summa);
		double npros = nolla*100.0/(summa);
	//	System.out.println(mpros);
		System.out.println(npj +"N"+pjd+" voittaa:   " + miinus + " eli " + mpros +"%");
		System.out.println(npe +"N"+ped+"+"+raising+" voittaa: "+ plus + " eli " + ppros+ "%");
		System.out.println("Tasapelit: " + nolla + " eli " + npros + "%");
		System.out.println();
	}
	
}
