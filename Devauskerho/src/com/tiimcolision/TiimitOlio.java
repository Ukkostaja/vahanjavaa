package com.tiimcolision;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Random;


public class TiimitOlio implements Comparable<TiimitOlio>{
	ArrayList<Tiimi> tiimit;
	ArrayList<Osallistuja> osallistujat;
	

	
	public TiimitOlio() {
		tiimit = new ArrayList<Tiimi>();
		osallistujat =  new ArrayList<Osallistuja>();
		this.readTeams();
		this.readPeople();
		
		this.jaaSatunnaisesti();
	}
	
	public void readTeams(){
		BufferedReader br;
		try {
			FileReader f = new FileReader("tiimit.txt");
			br = new BufferedReader(f);
			String temp;
			while((temp = br.readLine() ) != null) {
				tiimit.add(new Tiimi(temp));
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		tiimit.trimToSize();
	}
	
	
	public void breed() {
		Collections.shuffle(tiimit);
		for(int i = 0 ; i < tiimit.size()-1; i+=2){
			tiimit.get(i).breed(tiimit.get(i+1));
		}
	}
	
	public void readPeople(){
		BufferedReader br;
		try {
			FileReader f = new FileReader("osallistujat.txt");
			br = new BufferedReader(f);
		
			String temp;
			while((temp = br.readLine() ) != null) {
				osallistujat.add(new Osallistuja(temp));
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	
	public void print() {
		Collections.sort(tiimit);
		for( Tiimi t: tiimit){
			t.print();
		}
		System.out.println("*Total Happiness* "+this.happiness());
	}
	
	public void jaaSatunnaisesti(){
		Random rdom = new Random();
		for(Tiimi t : tiimit) {
			t.minJasenet = osallistujat.size()/tiimit.size();
		}
		for(Osallistuja o : osallistujat) {
			if(o != null) {
				int rnd = rdom.nextInt(tiimit.size());
				while(tiimit.get(rnd) == null) {
					System.out.print("*");
					rnd = rdom.nextInt(tiimit.size());
				}
				//System.out.print(rnd +" ");
				tiimit.get( rnd ).jasenet.add(o);
			}
		}
		//System.out.println();
	}
	
	public int happiness(){
		int happiness = 0;
		for(Tiimi t : tiimit){
			happiness += t.happiness();
		}
		return happiness;
	}
	
	public static void main(String[] args) {
		System.out.println("toimin");

	}

	@Override
	public int compareTo(TiimitOlio o) {
		return ( this.happiness() - o.happiness());
	}

}
