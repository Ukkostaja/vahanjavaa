package com.tiimcolision;

import java.util.ArrayList;

public class Osallistuja {
	String name;
	final static int happinessM = 5;
	final static int maxHappiness = 15;
	ArrayList<String> cando;
	ArrayList<String> wants;
	
	
	public int[] cabable(ArrayList<String> needed, int[] fullfilledNeeds){
		String need;
		for(int i = 0; i<needed.size();i++) {

			if (fullfilledNeeds[i] == 2) continue;
			need = needed.get(i);
			if(need == null) continue;
			for(String cd: cando){
				if(cd.contains(need)){
					fullfilledNeeds[i] = 2;
					return fullfilledNeeds;
				}
			}
			
		}

		return fullfilledNeeds;
	}
	
	public void print(){
		System.out.println(name);
	}
	
	
	public int happiness(Tiimi team) {
		for (String want : wants) {
			if(want.contains(team.name)){
				return maxHappiness - ( happinessM * wants.indexOf(want) ); 
			}
		
		}
		return 0;
		
	}
	
	
	Osallistuja(String parse){
		cando = new ArrayList<>();
		wants = new ArrayList<>();
		String[] palat = parse.split(";");
		this.name = palat[0];
		String[] candos = palat[1].split(",");
		for(String can: candos){
			if(can != null){
				cando.add(can);
			}
		}
		String[] wantos= palat[2].split(",");
		for(String want: wantos){
			if(want != null) {
				wants.add(want);
			}
		}
	}
	
}
