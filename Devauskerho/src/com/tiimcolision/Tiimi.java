package com.tiimcolision;
import java.util.ArrayList;
import java.util.Random;


public class Tiimi implements Comparable<Tiimi>{
	String name;
	ArrayList<String> needs;
	int[] fullfilledNeeds; // 0= not filled , 1= one part timer , 2 =1 fulltime or 2 part timers
	ArrayList<Osallistuja> jasenet;
	int minJasenet=0;
	
	public int happiness(){
		int happiness = 0;
		int howmany;
		for(Osallistuja o : jasenet){
			fullfilledNeeds = o.cabable(needs, fullfilledNeeds);
			happiness +=o.happiness(this);
		}
		for(int i = 0; i< fullfilledNeeds.length;i++){
			if(fullfilledNeeds[i] == 2) happiness+=100;
		}
		
		
		if(jasenet.size() > minJasenet){
			happiness-= 75* (jasenet.size()-minJasenet);
		}
		return happiness;
	}
	
	public void print() {
		System.out.println(name +" ("+ this.happiness()+ "HP)");
		for(Osallistuja o : jasenet){
			System.out.print("      ");
			System.out.println(o.name);
		}
	}
	
	public void breed(Tiimi toinen){
		int koko;
		Random rdom = new Random();
		if(jasenet.size() <= toinen.jasenet.size()) { //valitaan pienemm�n listan koko
			koko = jasenet.size();
		} else {
			koko = toinen.jasenet.size();
		}
		int kohta;
		if(koko == 0) {
			kohta = 0;
		} else {
			kohta = rdom.nextInt(koko);	//valitaan satuinnaskohta
		}
		ArrayList<Osallistuja> eka = new ArrayList<Osallistuja>();
		ArrayList<Osallistuja> toka = new ArrayList<Osallistuja>();
		for(int i =0; i < kohta; i++){// lis�t��n ennen kohtaa tulevat alkiot uusiin listoihin.
			eka.add(jasenet.get(i));
			toka.add(toinen.jasenet.get(i));
		}
		jasenet.removeAll(eka);	//poistetaan otetut alkiot vanhasta listasta
		toinen.jasenet.removeAll(toka);
		
		jasenet.addAll(toka);  //Lis�t��n uudet alkiot
		toinen.jasenet.addAll(eka);
		
		jasenet.trimToSize(); //Poistetaan ylim��r�iset nullit
		toinen.jasenet.trimToSize();
	}
	
	Tiimi(String parse){
		needs = new ArrayList<>();
		jasenet = new ArrayList<>();
		String[] palat = parse.split(";");
		this.name = palat[0];
		String[] needsStrings = palat[1].split(",");
		for(String need : needsStrings ) {
			needs.add(need);
		}
		fullfilledNeeds = new int[needs.size()];
		
		
		
	}

	@Override
	public int compareTo(Tiimi t) {
		return this.name.compareTo(t.name);
	}

}
