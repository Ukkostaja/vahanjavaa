package com.tiimcolision;


import java.util.ArrayList;
import java.util.Collections;

public class PopulationControl {
	ArrayList<TiimitOlio> oliot;
	public int montako;
	public int kierroksia;
	public int kierros;
	
	public PopulationControl(){
		this(1000,10);
	}
	
	public PopulationControl(int montako, int kierroksia) {
		System.out.print("Initializing");
		this.kierroksia = kierroksia;
		this.montako = montako;
		oliot = new ArrayList<>(montako);
		for(int i= 0;i<montako;i++){
			oliot.add(new TiimitOlio());
			if(i%100 == 0) System.out.print(".");
		}
		System.out.println("Done");
		System.out.print("Initial Sort...");
		this.sortOliot();
		System.out.println("Done");
		
	}

	public void sortOliot(){
		Collections.sort(oliot);
		Collections.reverse(oliot);
	}
	
	public void sortOliot(ArrayList<TiimitOlio> to){
		Collections.sort(to);
		Collections.reverse(to);
	}
	
	private void breeding() {
		//kierros++;
		int halfpoint=oliot.size()/2;
		ArrayList<TiimitOlio> uusoliot = new ArrayList<>(oliot.size());
		
		for(int i = 0; i< halfpoint;i++){
			uusoliot.add(oliot.get(i));
		}
		for(int i = 0; i< halfpoint;i++){
			uusoliot.add(oliot.get(i));
		}
		for(int i = 0; i< uusoliot.size()/2;i++){
			uusoliot.get(i).breed();
		}

		
		oliot = uusoliot;
		this.sortOliot();
		
		
		
	}
	
	
	public void breed(){
		for(kierros =0;kierros < kierroksia; kierros++){
			System.out.println("Lap:"+kierros+" start");
			this.breeding();
			System.out.println("Lap:"+kierros+" end");
		}
	}
	
	public void printBest(int number) {
		for(int i=number; i > 0;i--){
			oliot.get(i).print();
			System.out.println("-----------");
		}
	}
	
	public static void printHelp(){
		System.out.println("T�m� ohjelma pyrkii luomaan mahdollisimman hyvi� tiimej�.");
		System.out.println("Ohjelmaa k�ytet��n parametrien avulla, jotka annetaan M��R�TYSS� j�rjestyksess�.");
		System.out.println("Parametrit ovat pelkki� numeroita. Parametrit ovat seuraavat, seuraavassa j�rjestyksess�.");
		System.out.println("-tiimikombinaatioiden m��r� eli monta eri vaihtoehtoa pidet��n mukana. Oletus:1000");
		System.out.println("-montako kertaa tulosta yrit��n parannella. 0 tarkoittaa t�ysin satunnaista. Oletus: 10");
		System.out.println("-montako parasta vaihtoehtoa n�ytet��n. Oletus: 1");
		System.out.println("Oletusarvot eiv�t ole v�ltt�m�tt� hyvi� ja niiden sopivuus riippuu tilanteesta.");
		System.out.println("Ne ovat t�ss� vain kertaluokan ilmaisemista varten.");
		System.out.println("Mit� isompia numeroita k�ytet��n parametreina sit� parempia tulokset ovat, mutta my�s sit� kauemmin ohjelma suorittaminen kest��.");
		System.out.println("Jos et saa tuloksia niin tarkista, ett� kaikki tiedostoissa k�ytetyt nimet vastaavat toisiaan T�YDELLISESTI.");
		System.out.println("Esim. Grafiikka ei ole sama kuin Graafikko tai grafiikka. T�m� p�tee my�s tiimien nimiin!");
		System.out.println("Ohjelma lukee samassa kansiossa olevat tiedostot 'tiimit.txt' ja 'osallistujat.txt'.");
		System.out.println("Ohjelman tehnyt Kari Tuurihalme.");
	}
	
	public static void main(String[] args) {
		/*for(int i = 0; i<args.length;i++){
			System.out.println(args[i]);
		}*/
		int montako, kierroksia,results;
		PopulationControl pc;
		switch (args.length) {
		case 3:
			montako = Integer.parseInt(args[0]);
			kierroksia = Integer.parseInt(args[1]);
			results = Integer.parseInt(args[2]);
			pc = new PopulationControl(montako,kierroksia);
			break;
		case 1:
			if(args[0].contains("-h")) {
				PopulationControl.printHelp();
				System.exit(0);
			}
			results = Integer.parseInt(args[0]);
			pc = new PopulationControl();
			break;
		default:
			
			pc = new PopulationControl();
			results =1;
			
			break;
		}
		
		pc.breed();
		pc.printBest(results);
		System.out.println("K�ynnist� -h parametrill� saadaksesi ohjeen.");
	}

}
