package javaeclipse;

import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.Point;
import java.util.ArrayList;

public class HexCanvas extends Canvas {
	
	ArrayList<Hex> hexes;
	double s = 70.0;
	double d = Math.sqrt(3)/2*s;
	int di = (int)d+1;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public HexCanvas(int size) {
		hexes = new ArrayList<Hex>(size);
		int ox = this.getWidth() / 2;
		int oy = this.getHeight() /2;
		System.out.println(ox + " " + oy);
		int i=0;
		int keha;
		int fb;
		int[] temp;
		while(i<size){
			temp =  HexPoints.getKeha(i);
			keha = temp[0];
			fb = temp[1];
			int n = keha*6;
			int tama = i-fb;
			double alfa = 2*Math.PI*tama/n;
			int y = (int) (Math.sin(alfa)*keha*d);
			int x = (int) (Math.cos(alfa)*keha*d);
			System.out.println(x + ","+y);
			hexes.add(i, new Hex(new Point(ox+x,oy+y), s));
			
			i++;
			
		}
		System.out.println(hexes.size());
		//this.setSize(800, 600);
	}
	
	public void update(){
		int ox = this.getWidth() / 2;
		int oy = this.getHeight() /2;
		System.out.println(ox + " " + oy);
		int i=0;
		int keha;
		int fb;
		int x=0;
		int y=0;
		int[] temp;
		Hex hex;
		Point tp;
		while(i<this.hexes.size()){
			hex = this.hexes.get(i);
			if(hex != null){
				temp =  HexPoints.getKeha(i);
				keha = temp[0];
				fb = temp[1];
				int n = keha*6;
				int tama = i-fb;
				double alfa = 2*Math.PI*tama/n;
				//System.out.println(alfa);
				y = (int) (Math.cos(alfa)*keha*d*2);
				x = (int) (Math.sin(alfa)*keha*d*2);
				tp = new Point(ox+x,oy-y);
				//System.out.println(tp);
				hex.updatecoor(HexPoints.getXs(tp, s), HexPoints.getYs(tp, s));
			}
			
			i++;
			
		}
	}
	
	public void setHexes(ArrayList<Hex> hexes){
		this.hexes = hexes;
	}
	@Override
	public void paint(Graphics win) {
		// TODO Auto-generated method stub
		super.paint(win);
		this.update();
		Hex temp;
		int size = this.hexes.size();
		System.out.println(" " +size);
		for(int i = 0; i<size;i++){
			temp = this.hexes.get(i);
			if (temp != null ){
				win.drawPolygon(temp);
			}
		}

		
		
		
		/*win.drawRect(0,0,200,200);
		win.drawPolygon(new Hex(new Point(x, y),s));
		win.drawPolygon(new Hex(new Point(x,y+2*di),s));
		win.drawPolygon(new Hex(new Point((int) (x+1.5*s) , y+di),s));
		win.drawPolygon(new Hex(new Point(x, y+300),s));
		*/
	}
}
