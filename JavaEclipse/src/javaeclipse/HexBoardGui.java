package javaeclipse;

import javax.swing.JFrame;

public class HexBoardGui extends JFrame{
	HexCanvas hc;
	
	
	public HexBoardGui() {

	super("Eclipse Board");
	this.setDefaultCloseOperation(EXIT_ON_CLOSE);
	this.setSize(1200, 1000);
	this.hc = new HexCanvas(37);
	
	this.getContentPane().add(this.hc);
	
	this.setVisible(true);
	}
}


/*
2: 7, 13
3: 7,11,15
4: 7,9,13,15
5: 7,9,11,15,17
6: 7,9,11,13,15,16
*/