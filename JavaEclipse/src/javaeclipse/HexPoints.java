package javaeclipse;

import java.awt.Point;

public class HexPoints {

	int n = 6;


	public static int[] getKeha(int index){
		int keha =0;
		int first = 1;
		int fb;
		if(index>0){
			while(true){
				fb = first;
				first = first+6*keha;
				if(index <first){
					int[] tulos = {keha,fb};
					return tulos;
				} else {
					keha++;
				}
			}
		}
		int[] tulos = {0,0};
		return tulos;
	}
	
	
	public static int[] getYs(Point p, double r) {
		double y = Math.cos(Math.PI/6)*r;
		int[] tys = {
				(int) (p.y-y),
				(int) (p.y-y),
				p.y,
				(int) (p.y+y),
				(int) (p.y+y),
				p.y	
		};
		return tys;
	}
	
	public static int[] getXs(Point p, double r) {
			int x = (int) (r/2);
			int[] txs = {
					(p.x-x),
					(p.x+x),
					(int) (p.x+r),
					(p.x+x),
					(p.x-x),
					(int) (p.x-r)
			};
			return txs;
		
	}
}
