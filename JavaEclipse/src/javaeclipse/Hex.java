package javaeclipse;

import java.awt.Point;
import java.awt.Polygon;

public class Hex extends Polygon{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	int[] xs;
	int[] ys;
	double r;
	
	boolean redraw= true;
	int owner;
	int scicu;
	int ecocu;
	int matcu;
	boolean monolith;
	boolean orbital;
	Ships ships;
	
	public Hex(Point p, double r) {
		super(HexPoints.getXs(p, r), HexPoints.getYs(p, r),6);
		this.r = r;
	}
	public Hex(double r){
		this.r = r;
	}
	
	public void updatecoor(int[] xs, int[] ys) {
		this.xpoints = xs;
		this.ypoints = ys;
	}
}
