package pac;

public class Math {
	public static int convertToOneDigit(int two) {
		if (two > 9) {
				int mod = two % 10;
				return convertToOneDigit(two / 10 + mod);
		}
		else return two;
	}
	
	public static int StringToInt(String merkki) {
		return Integer.parseInt(merkki);
	}
	
	public static String IntToString(int luku){
		return Integer.toString(luku);
	}

}
