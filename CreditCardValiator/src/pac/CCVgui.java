package pac;

import java.awt.*;
import java.awt.event.*;


public class CCVgui extends Frame {
	TextArea ta;
	
	public CCVgui() {
		super("CCV");
		this.setMinimumSize(new Dimension(640, 480));
		FlowLayout flo = new FlowLayout();
		this.setLayout(flo);
		this.ta = new TextArea();
		this.ta.setSize(new Dimension(640, 480));
		this.add(ta);
		this.setVisible(true);
		this.addWindowListener(new WindowAdapter() {
	           public void windowClosing(WindowEvent e) {
	               e.getWindow().dispose();
	               }
		
		});
	}
	
	public void getline(String rivi) {
		this.ta.append(rivi);
	}

}
