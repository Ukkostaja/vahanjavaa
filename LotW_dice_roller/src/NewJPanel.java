
import java.awt.BorderLayout;

import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagLayout;
import javax.swing.JButton;

import javax.swing.WindowConstants;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
* This code was edited or generated using CloudGarden's Jigloo
* SWT/Swing GUI Builder, which is free for non-commercial
* use. If Jigloo is being used commercially (ie, by a corporation,
* company or business for any purpose whatever) then you
* should purchase a license for each developer using Jigloo.
* Please visit www.cloudgarden.com for details.
* Use of Jigloo implies acceptance of these licensing terms.
* A COMMERCIAL LICENSE HAS NOT BEEN PURCHASED FOR
* THIS MACHINE, SO JIGLOO OR THIS CODE CANNOT BE USED
* LEGALLY FOR ANY CORPORATE OR COMMERCIAL PURPOSE.
*/
public class NewJPanel extends javax.swing.JPanel {
	private JLabel jLabel1;
	private JButton jButton1;
	private JButton jButton2;
	private JLabel jLabel3;
	private JLabel jLabel2;

	/**
	* Auto-generated main method to display this 
	* JPanel inside a new JFrame.
	*/
	public static void main(String[] args) {
		JFrame frame = new JFrame();
		frame.getContentPane().add(new NewJPanel());
		frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		frame.pack();
		frame.setVisible(true);
	}
	
	public NewJPanel() {
		super();
		initGUI();
	}
	
	private void initGUI() {
		try {
			FlowLayout thisLayout = new FlowLayout();
			this.setPreferredSize(new java.awt.Dimension(327, 99));
			BorderLayout thisLayout1 = new BorderLayout();
			this.setLayout(thisLayout1);
			{
				jLabel2 = new JLabel();
				this.add(jLabel2, BorderLayout.EAST);
				jLabel2.setText("jLabel2");
			}
			{
				jLabel3 = new JLabel();
				this.add(jLabel3, BorderLayout.WEST);
				jLabel3.setText("jLabel3");
			}
			{
				jButton1 = new JButton();
				this.add(jButton1, BorderLayout.SOUTH);
				jButton1.setText("jButton1");
			}
			{
				jButton2 = new JButton();
				this.add(jButton2, BorderLayout.NORTH);
				jButton2.setText("jButton2");
			}
			{
				jLabel1 = new JLabel();
				this.add(jLabel1, BorderLayout.CENTER);
				GridBagLayout jLabel1Layout = new GridBagLayout();
				jLabel1Layout.rowWeights = new double[] {0.1, 0.1, 0.1, 0.1};
				jLabel1Layout.rowHeights = new int[] {7, 7, 7, 7};
				jLabel1Layout.columnWeights = new double[] {0.1, 0.1, 0.1, 0.1};
				jLabel1Layout.columnWidths = new int[] {7, 7, 7, 7};
				jLabel1.setLayout(jLabel1Layout);
				jLabel1.setText("jLabel1");
				jLabel1.setPreferredSize(new java.awt.Dimension(53, 72));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
