import java.awt.Component;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.SwingConstants;


public class RollerPanel extends JPanel implements ActionListener {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	Roller roller;
	ArrayList<JLabel> lake;
	ArrayList<JLabel> river;
	
	RollerPanel(Roller roller){
		super();
		this.roller = roller;
		JPanel pane = this;
		this.setLayout(new GridBagLayout());
		GridBagConstraints c = new GridBagConstraints();
		lake= new ArrayList<JLabel>();river= new ArrayList<JLabel>();
		for(int i= 0; i < roller.lakesize;i++){
			JLabel sininen = new JLabel();
			lake.add(sininen);
			pane.add(sininen);
		};
		pane.add(new JSeparator(SwingConstants.VERTICAL),c);
		for(int i= 0; i < roller.riversize;i++) {
			JLabel keltainen = new JLabel();
			river.add(keltainen);
			pane.add(keltainen);
		}
		JButton roll = new JButton("Roll");
		roll.setActionCommand("roll");
		roll.addActionListener(this);
		pane.add(roll);
		this.setVisible(true);
		this.validate();
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		boolean found = false;
		String action= e.getActionCommand();
		if(action == "roll") {
			roll();
			found = true;
		}

		if(found== false){

			System.out.println(e.getActionCommand());

		}
		
	}
	
	public void update(){
		for(int i = 0; i < river.size(); i++ ) {
			JLabel temp = river.get(i);
			if( temp != null) {
				temp.setText(""+roller.river[i]);
			}	
		}
		for(int i = 0; i < lake.size(); i++ ) {
			JLabel temp = lake.get(i);
			if( temp != null) {
				temp.setText(""+roller.lake[i]);
			}
		}
		this.validate();
	}
	
	public void roll(){
		roller.roll();
		update();
	}

}
