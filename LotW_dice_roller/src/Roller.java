import java.util.Arrays;
import java.util.Random;

public class Roller {
	long lakesize;
	long riversize;
	int[] lake;
	int[] river;
	
	
	
	Roller(Rank rank){
		this.lakesize = rank.lakesize;
		this.riversize= rank.chiaura;
		lake = new int[(int) lakesize];
		river = new int[(int) riversize];
		clear();
	}
	
	public void roll(){
		Random rdom = new Random();
		for(int i = 0;i < lakesize; i++){
			lake[i] = rdom.nextInt(10);
		}
		trimLake();
	}
	
	
	public void wash(){
		Arrays.fill(river, -1);
		/*
		for(int i = 0; i< riversize;i++){
			river[i] = -1;
		}
		*/
	}
	
	public int flood(int index){
		int arvo = lake[index];
		lake[index] = -1;
		trimRiver();
		return arvo;

	}
	
	public int riverEmpty() {
		int arvo = 0;
		for(int i = 0 ; i < riversize; i++){
			if(river[i] == -1) {
				arvo++;
			}
		}
		return arvo;
	}
	
	private void trimRiver(){
		Arrays.sort(river);
	}
	
	private void trimLake(){
		Arrays.sort(lake);
	}
	
	public void trimAll(){
		trimLake();
		trimRiver();
	}
	
	public void floatd(int index, int offset) {
		//from lake to river
		System.arraycopy(lake, index, river, 0, offset);
		Arrays.fill(lake, index, index+offset, -1);
		trimAll();
		
		
	}
	
	public void flow(int index, int offset){
		//from river to lake
		int[] result = new int[lake.length+offset];
		System.arraycopy(lake, 0, result, 0, lake.length);
		System.arraycopy(river, index, result, lake.length, offset);
		for(int i = 0; i < offset; i++){
			river[i+index] = -1;
		}
		this.lake = result;
		trimAll();
	}
	
	public void clear(){
		Arrays.fill(lake, -1);
		/*
		for(int i = 0; i< lakesize;i++){
			lake[i] = -1;
		}
		*/
		
		wash();
	}
	
	
}
