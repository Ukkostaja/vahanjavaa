import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

import javax.swing.*;


public class GUI extends JFrame {
	static final long serialVersionUID = 1;
	Container mainu;
	ArrayList<Roller> rollers;
	ArrayList<JLabel> labels;
	
	GUI() {
		super("Legends of the Wulin Dice roller");
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.mainu = this.getContentPane();
		BoxLayout bl = new BoxLayout(mainu, BoxLayout.Y_AXIS);
		
		
		JButton koe = new JButton("koe!");
		JButton yksi = new JButton("1");
		

		
		
		koe.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent e) {
				System.out.println("toimii");
				
				//addLocalRoller();
				update();
				
			}
		});
		mainu.add(koe);
		mainu.add(yksi);
		
		this.setPreferredSize(new Dimension(800, 600));
		
		this.setLayout(bl);
		this.pack();
		this.setVisible(true);
	}


	public void update() {
		this.pack();
	}
	
	public void addRollerPanel(Roller roller){
		RollerPanel rp = new RollerPanel(roller);
		
		rp.setVisible(true);
		mainu.add(rp);
		update();
	}

	





}
