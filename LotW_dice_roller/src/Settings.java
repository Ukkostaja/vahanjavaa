
public final class Settings {
	
	public static int[] dcNumber = {10,20,30,40,60,80,100};
	public static String[] dcText = {"Trivial","Moderate","Hard","Memorable","Fantastic","Legendary","Impossible"};
	public static Rank[] ranks = {new Rank(1),new Rank(5), new Rank(4), new Rank(3), new Rank(2),new Rank(1)};
}
